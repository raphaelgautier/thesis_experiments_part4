# Thesis Experiments - Part 4

This repository contains the implementation of experiments 3.1, 3.2, and 4 from the thesis entitled "Bayesian, Gradient-Free, and Multi-Fidelity Supervised Dimension Reduction Methods For Surrogate Modeling Of Expensive Analyses With High-Dimensional Inputs".

## Related Repositories

 - [Thesis Experiments Part 1](https://gitlab.com/raphaelgautier/thesis_experiments_part1)
 - [Thesis Experiments Part 2](https://gitlab.com/raphaelgautier/thesis_experiments_part2)
 - [Thesis Experiments Part 3](https://gitlab.com/raphaelgautier/thesis_experiments_part3)
