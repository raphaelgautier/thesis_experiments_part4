from setuptools import setup, find_packages

with open("requirements.txt", "r") as requirement_txt:
    requirements = [line for line in requirement_txt.readlines()]

setup(
    name="ThesisExperimentsPart4",
    version="0.0.1",
    url="https://gitlab.com/raphaelgautier/thesis_experiments_part4",
    author="Raphaël Gautier",
    author_email="raphael.gautier@gatech.edu",
    description="Fourth and final part of the code implementing thesis experiments.",
    packages=find_packages(),
    package_data={'exp4.elliptic_pde': ['*.npz']},
    install_requires=requirements,
    python_requires="==3.7.*",
)
