from typing import Dict, Tuple

from jax import vmap, numpy as jnp
from jax.scipy.linalg import cho_solve, cho_factor, solve_triangular
import numpy as np
from numpyro import enable_x64, sample, deterministic, distributions as dist

from exp4.utils.dataset import Dataset

from .utils.gp import se_kernel

enable_x64()

###################################
# Numpyro model used for training #
###################################


def gp_kernel(
    x_1: np.ndarray, x_2: np.ndarray, model_parameters: Dict[str, np.ndarray]
) -> np.ndarray:
    """Kernel for the GP."""
    return se_kernel(
        x_1,
        x_2,
        model_parameters["signal_variance"],
        model_parameters["length_scales"],
    )


def get_priors(dim_inputs: int) -> Dict:
    model_parameters = {
        "signal_variance": sample("signal_variance", dist.HalfNormal(1)),
        "dim_weights": sample(
            "dim_weights",
            dist.HalfNormal(jnp.ones((dim_inputs,))),
        ),
        "noise_variance": sample("noise_variance", dist.HalfNormal(1)),
    }
    model_parameters["length_scales"] = deterministic(
        "length_scales", 1 / jnp.abs(model_parameters["dim_weights"])
    )

    return model_parameters


def get_alternate_priors(dim_inputs: int) -> Dict:
    return {
        "signal_variance": sample("signal_variance", dist.LogNormal(0, 1)),
        "length_scales": sample(
            "length_scales",
            dist.LogNormal(jnp.zeros((dim_inputs,)), jnp.ones((dim_inputs,))),
        ),
        "noise_variance": sample("noise_variance", dist.LogNormal(0, 1)),
    }


def bgp_model(
    training_data: Dataset, jitter: float = 1e-8, use_alternate_priors: bool = False
) -> None:
    # Extract training data
    obs_x = training_data.x
    obs_y = training_data.y

    # Extract problem dimensions
    num_samples = obs_x.shape[0]
    dim_inputs = obs_x.shape[1]

    # Priors
    if use_alternate_priors:
        model_parameters = get_alternate_priors(dim_inputs)
    else:
        model_parameters = get_priors(dim_inputs)

    # GP
    k_x_x = (
        gp_kernel(obs_x, obs_x, model_parameters)
        + model_parameters["noise_variance"] * jnp.eye(num_samples)
        + jitter * jnp.eye(num_samples)
    )
    lower_chol_k_x_x, _ = cho_factor(k_x_x, lower=True)
    sample(
        "obs_y",
        dist.MultivariateNormal(jnp.zeros((num_samples,)), scale_tril=lower_chol_k_x_x),
        obs=obs_y.flatten(),
    )


#######################
# Prediction Routines #
#######################

# Body of the loop over the prediction sites (`pred_x` is one element)
# Posterior draw fixed in the outer loop (`posterior_draw` is one element)
def bgp_predict_loop_2(
    pred_x: np.ndarray,
    posterior_draw: Dict[str, np.ndarray],
    training_data: Dict[str, np.ndarray],
    c_and_lower: Tuple[np.ndarray, bool],
    alpha: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    # Prediction site must be a row vector
    pred_x = pred_x[None, :]

    # Intermediary computations
    k_star = gp_kernel(training_data["x"], pred_x, posterior_draw)
    v = solve_triangular(c_and_lower[0], k_star, lower=c_and_lower[1])

    # Mean
    mean = (k_star.T @ alpha)[0, 0]

    # Variance
    variance = (gp_kernel(pred_x, pred_x, posterior_draw) - v.T @ v)[0, 0]

    return mean, variance


# Body of the loop over the posterior draws (`posterior_draw` is one element)
def bgp_predict_loop_1(
    pred_x: np.ndarray,
    training_data: Dict[str, np.ndarray],
    posterior_draw: Dict[str, np.ndarray],
    jitter: float = 1e-8,
) -> Tuple[np.ndarray, np.ndarray]:
    # Problem dimensions
    num_training_samples = training_data["x"].shape[0]

    # Covariance structure can be computed given posterior_draw
    k_x_x = (
        gp_kernel(training_data["x"], training_data["x"], posterior_draw)
        + posterior_draw["noise_variance"] * jnp.eye(num_training_samples)
        + jitter * jnp.eye(num_training_samples)
    )

    # Intermediary computations, valid for all prediction sites
    c_and_lower = cho_factor(k_x_x, lower=True)
    alpha = cho_solve(c_and_lower, training_data["y"].reshape((-1, 1)))

    # Next step is to loop over the prediction sites
    return vmap(bgp_predict_loop_2, in_axes=(0, None, None, None, None))(
        pred_x, posterior_draw, training_data, c_and_lower, alpha
    )
