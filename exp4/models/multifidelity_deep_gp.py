from typing import Dict, Tuple

from jax import numpy as jnp, vmap
from jax.scipy.linalg import cholesky, cho_solve, cho_factor, solve_triangular
import numpy as np
from numpyro import enable_x64, distributions as dist, sample, deterministic

from exp4.utils import Dataset

from .utils.gp import se_kernel


enable_x64()

###########
# Kernels #
###########


def lf_kernel(
    x_1: np.ndarray, x_2: np.ndarray, model_parameters: Dict[str, np.ndarray]
) -> np.ndarray:
    """Kernel for the LF GP."""
    return se_kernel(
        x_1,
        x_2,
        model_parameters["lf_signal_variance"],
        model_parameters["lf_length_scales"],
    )


def hf_kernel(
    x_1: np.ndarray, x_2: np.ndarray, model_parameters: Dict[str, np.ndarray]
) -> np.ndarray:
    """Kernel for the HF GP."""
    return se_kernel(
        x_1[:, :-1],
        x_2[:, :-1],
        model_parameters["rho_signal_variance"],
        model_parameters["rho_length_scales"],
    ) * (
        model_parameters["linear_variance"] * x_1[:, None, -1] @ x_2[:, None, -1].T
        + se_kernel(
            x_1[:, None, -1],
            x_2[:, None, -1],
            model_parameters["hf_lf_signal_variance"],
            model_parameters["hf_lf_length_scale"],
        )
    ) + se_kernel(
        x_1[:, :-1],
        x_2[:, :-1],
        model_parameters["delta_signal_variance"],
        model_parameters["delta_length_scales"],
    )


#########
# Model #
#########


def get_priors(dim_inputs: int) -> Dict:
    model_parameters = {
        # 💡 LF GP
        "lf_signal_variance": sample("lf_signal_variance", dist.HalfNormal(1)),
        "lf_dim_weights": sample(
            "lf_dim_weights",
            dist.HalfNormal(jnp.ones((dim_inputs,))),
        ),
        "lf_noise_variance": sample("lf_noise_variance", dist.HalfNormal(1)),
        # "lf_noise_variance": deterministic("lf_noise_variance", 0),
        # 💡 HF GP
        # rho component
        "rho_signal_variance": sample("rho_signal_variance", dist.HalfNormal(1)),
        # "rho_signal_variance": deterministic("rho_signal_variance", 1.0),
        "rho_dim_weights": sample(
            "rho_dim_weights",
            dist.HalfNormal(jnp.ones((dim_inputs,))),
        ),
        # linear component
        "linear_variance": sample("linear_variance", dist.HalfNormal(1)),
        # lf component
        "hf_lf_signal_variance": sample("hf_lf_signal_variance", dist.HalfNormal(1)),
        # "hf_lf_signal_variance": deterministic("hf_lf_signal_variance", 0),
        "hf_lf_dim_weights": sample("hf_lf_dim_weights", dist.HalfNormal(1)),
        # delta component
        "delta_signal_variance": sample("delta_signal_variance", dist.HalfNormal(1)),
        "delta_dim_weights": sample(
            "delta_dim_weights",
            dist.HalfNormal(jnp.ones((dim_inputs,))),
        ),
        "hf_noise_variance": sample("hf_noise_variance", dist.HalfNormal(1)),
        # "hf_noise_variance": deterministic("hf_noise_variance", 0),
    }

    model_parameters.update(
        {
            "lf_length_scales": deterministic(
                "lf_length_scales", 1 / jnp.abs(model_parameters["lf_dim_weights"])
            ),
            "rho_length_scales": deterministic(
                "rho_length_scales", 1 / jnp.abs(model_parameters["rho_dim_weights"])
            ),
            "hf_lf_length_scale": deterministic(
                "hf_lf_length_scale", 1 / jnp.abs(model_parameters["hf_lf_dim_weights"])
            ),
            "delta_length_scales": deterministic(
                "delta_length_scales",
                1 / jnp.abs(model_parameters["delta_dim_weights"]),
            ),
        }
    )

    return model_parameters


def get_alternate_priors(dim_inputs: int) -> Dict:
    return {
        # 💡 LF GP
        "lf_signal_variance": sample("lf_signal_variance", dist.LogNormal(0, 1)),
        "lf_length_scales": sample(
            "lf_length_scales",
            dist.LogNormal(jnp.zeros((dim_inputs,)), jnp.ones((dim_inputs,))),
        ),
        "lf_noise_variance": sample("lf_noise_variance", dist.LogNormal(0, 1)),
        # 💡 HF GP
        # rho component
        "rho_signal_variance": sample("rho_signal_variance", dist.LogNormal(0, 1)),
        "rho_length_scales": sample(
            "rho_length_scales",
            dist.LogNormal(jnp.zeros((dim_inputs,)), jnp.ones((dim_inputs,))),
        ),
        # linear component
        "linear_variance": sample("linear_variance", dist.LogNormal(0, 1)),
        # lf component
        "hf_lf_signal_variance": sample("hf_lf_signal_variance", dist.LogNormal(0, 1)),
        "hf_lf_length_scale": sample("hf_lf_length_scale", dist.LogNormal(0, 1)),
        # delta component
        "delta_signal_variance": sample("delta_signal_variance", dist.LogNormal(0, 1)),
        "delta_length_scales": sample(
            "delta_length_scales",
            dist.LogNormal(jnp.zeros((dim_inputs,)), jnp.ones((dim_inputs,))),
        ),
        "hf_noise_variance": sample("hf_noise_variance", dist.LogNormal(0, 1)),
    }


def mf_deep_gp_model(
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    jitter: float = 1e-6,
    use_alternate_priors: bool = False,
):
    # Preliminaries ####################################################################

    # Unbundle training data
    obs_lf_x = lf_training_data.x
    obs_lf_y = lf_training_data.y
    obs_hf_x = hf_training_data.x
    obs_hf_y = hf_training_data.y

    # Locations of all observations (LF first, then HF)
    obs_x = jnp.concatenate((obs_lf_x, obs_hf_x), axis=0)

    # Problem dimensions
    num_lf_obs = obs_lf_x.shape[0]
    num_hf_obs = obs_hf_x.shape[0]
    num_total_obs = num_lf_obs + num_hf_obs
    dim_inputs = obs_lf_x.shape[1]

    # Priors for the Model Parameters ##################################################
    if use_alternate_priors:
        model_parameters = get_alternate_priors(dim_inputs)
    else:
        model_parameters = get_priors(dim_inputs)

    # LF GP Model ######################################################################
    # LF GP outputs are modeled at the locations of both the LF and HF observations

    # Kernel matrix
    k_lf_x = lf_kernel(obs_x, obs_x, model_parameters) + jitter * jnp.eye(num_total_obs)
    lower_chol_k_lf_x = cholesky(k_lf_x, lower=True)

    # Latent GP `f` (before adding noise)
    lf_f = sample(
        "lf_f",
        dist.MultivariateNormal(
            jnp.zeros((num_total_obs,)), scale_tril=lower_chol_k_lf_x
        ),
    )

    # Model for the LF observations `y_LF`
    sample(
        "obs_lf_y",
        dist.Normal(
            loc=lf_f[:num_lf_obs],
            scale=jnp.sqrt(model_parameters["lf_noise_variance"]),
        ),
        obs=obs_lf_y.flatten(),
    )

    # ⚠ do not add noise to the LF values we feed to HF
    # Non-observed values of the LF model at the locations where HF is observed
    # lf_y_at_hf = sample(
    #     "lf_y_at_hf",
    #     dist.Normal(
    #         loc=f_lf[num_lf_samples:],
    #         scale=jnp.sqrt(model_parameters["lf_noise_variance"]),
    #     ),
    # )

    # Non-observed values of the LF model at the locations where HF is observed
    lf_f_at_hf: np.ndarray
    lf_f_at_hf = deterministic("lf_f_at_hf", lf_f[num_lf_obs:])  # type: ignore

    # HF GP ############################################################################

    # Augment HF inputs with the LF predictions
    hf_x_a = jnp.concatenate((obs_hf_x, lf_f_at_hf.reshape((-1, 1))), axis=1)

    # Model for the HF observations
    k_hf_hf_x_a_hf_x_a = (
        hf_kernel(hf_x_a, hf_x_a, model_parameters)
        + model_parameters["hf_noise_variance"] * jnp.eye(num_hf_obs)
        + jitter * jnp.eye(num_hf_obs)
    )
    lower_chol_k_hf_hf_x_a_hf_x_a = cholesky(k_hf_hf_x_a_hf_x_a, lower=True)
    sample(
        "obs_hf_y",
        dist.MultivariateNormal(
            jnp.zeros((num_hf_obs,)), scale_tril=lower_chol_k_hf_hf_x_a_hf_x_a
        ),
        obs=obs_hf_y.flatten(),
    )


##############
# Prediction #
##############

# LF prediction functions ##############################################################


# Body of the loop over the prediction sites (`pred_x` is one element)
# Posterior draw fixed in the outer loop (`posterior_draw` is one element)
def dmf_gp_lf_predict_loop_2(
    pred_x: np.ndarray,
    posterior_draw: Dict[str, np.ndarray],
    lf_training_data: Dict[str, np.ndarray],
    c_and_lower: Tuple[np.ndarray, bool],
    alpha: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    # Prediction site must be a row vector
    pred_x = pred_x[None, :]

    # Intermediary computations
    k_star = lf_kernel(lf_training_data["x"], pred_x, posterior_draw)
    v = solve_triangular(c_and_lower[0], k_star, lower=c_and_lower[1])

    # Mean
    mean = (k_star.T @ alpha)[0, 0]

    # Variance
    variance = (lf_kernel(pred_x, pred_x, posterior_draw) - v.T @ v)[0, 0]

    return mean, variance


# Body of the loop over the posterior draws (`posterior_draw` is one element)
def dmf_gp_lf_predict_loop_1(
    pred_x: np.ndarray,
    posterior_draw: Dict[str, np.ndarray],
    lf_training_data: Dict[str, np.ndarray],
    jitter: float = 1e-8,
) -> Tuple[np.ndarray, np.ndarray]:
    # Problem dimensions
    num_training_samples = lf_training_data["x"].shape[0]

    # Covariance structure can be computed given posterior_draw
    k_x_x = (
        lf_kernel(lf_training_data["x"], lf_training_data["x"], posterior_draw)
        + posterior_draw["lf_noise_variance"] * jnp.eye(num_training_samples)
        + jitter * jnp.eye(num_training_samples)
    )

    # Intermediary computations, valid for all prediction sites
    c_and_lower = cho_factor(k_x_x, lower=True)
    alpha = cho_solve(c_and_lower, lf_training_data["y"])

    # Next step is to loop over the prediction sites
    return vmap(dmf_gp_lf_predict_loop_2, in_axes=(0, None, None, None, None))(
        pred_x, posterior_draw, lf_training_data, c_and_lower, alpha
    )


# HF prediction functions ##############################################################


# Body of the loop over the low-fidelity y samples (`lf_y_sample` is one element)
# Prediction site fixed in the outer loop (`pred_x` is one element)
# Posterior draw fixed in the outer loop (`posterior_draw` is one element)
# Note: `lf_y_samples` is now a single element
def dmf_gp_hf_predict_loop_3(
    pred_x, lf_y_sample, posterior_draw, hf_x_a, c_and_lower, alpha
) -> Tuple[np.ndarray, np.ndarray]:
    # Prediction site "augmented" with a sample from the LF GP's output
    # at that prediction site
    pred_x_a = jnp.concatenate((pred_x, lf_y_sample[None]))[None, :]

    # Intermediary computations
    k_star = hf_kernel(hf_x_a, pred_x_a, posterior_draw)
    v = solve_triangular(c_and_lower[0], k_star, lower=c_and_lower[1])

    # Mean
    mean = (k_star.T @ alpha)[0, 0]

    # Variance
    variance = (hf_kernel(pred_x_a, pred_x_a, posterior_draw) - v.T @ v)[0, 0]

    return mean, variance


# Body of the loop over the prediction sites (`pred_x` is one element)
# Posterior draw fixed in the outer loop (`posterior_draw` is one element)
# Note: `lf_y_samples` is now size (num_lf_samples)
def dmf_gp_hf_predict_loop_2(
    pred_x: np.ndarray,
    lf_y_samples: np.ndarray,
    posterior_draw: Dict[str, np.ndarray],
    hf_x_a: np.ndarray,
    c_and_lower: Tuple[np.ndarray, bool],
    alpha: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    # Loop over the low-fidelity y samples `lf_y_samples`
    return vmap(dmf_gp_hf_predict_loop_3, in_axes=(None, 0, None, None, None, None))(
        pred_x, lf_y_samples, posterior_draw, hf_x_a, c_and_lower, alpha
    )


# Body of the loop over the posterior draws (`posterior_draw` is one element)
# Note: `lf_y_samples` is now size (num_predictions x num_lf_samples)
def dmf_gp_hf_predict_loop_1(
    norm_pred_x: np.ndarray,
    lf_y_samples: np.ndarray,
    posterior_draw: Dict[str, np.ndarray],
    hf_training_data: Dict[str, np.ndarray],
    jitter=1e-8,
) -> Tuple[np.ndarray, np.ndarray]:
    # Problem dimensions
    num_training_samples = hf_training_data["x"].shape[0]

    # High-fidelity training sites are "augmented" with a sample of the LF GP's output
    # at the high-fidelity training sites
    hf_x_a = jnp.concatenate(
        (hf_training_data["x"], posterior_draw["lf_f_at_hf"][:, None]), axis=1
    )

    # Intermediary computations, valid for all prediction sites
    k_x_x = (
        hf_kernel(hf_x_a, hf_x_a, posterior_draw)
        + posterior_draw["hf_noise_variance"] * jnp.eye(num_training_samples)
        + jitter * jnp.eye(num_training_samples)
    )
    c_and_lower = cho_factor(k_x_x, lower=True)
    alpha = cho_solve(c_and_lower, hf_training_data["y"])

    # Loop over the prediction sites `pred_x`
    return vmap(dmf_gp_hf_predict_loop_2, in_axes=(0, 0, None, None, None, None))(
        norm_pred_x, lf_y_samples, posterior_draw, hf_x_a, c_and_lower, alpha
    )
