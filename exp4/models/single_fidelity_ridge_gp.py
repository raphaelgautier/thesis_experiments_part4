from dataclasses import dataclass
from functools import partial
from pathlib import Path
from typing import Optional, Dict, Tuple

from jax import jit, numpy as jnp
from jax.lax import map as jmap
import numpy as np
from numpyro import enable_x64, sample, distributions as dist

from exp4.utils import Dataset, Struct

from .single_fidelity_gp import bgp_model, bgp_predict_loop_1
from .utils.normalization import (
    NormalizationConstants,
    apply_dataset_normalization,
    normalize,
    compute_and_apply_dataset_normalization,
    denormalize,
)
from .utils.householder import householder_reparameterization
from .utils.mcmc import mcmc, McmcParameters

enable_x64()


###################################
# Numpyro model used for training #
###################################


def bfs_model(
    training_data: Dataset, dim_feature_space: int, use_alternate_priors: bool = False
) -> None:
    # Problem dimensions
    m = dim_feature_space
    n = training_data.x.shape[1]
    num_projection_parameters = m * n - m * (m - 1) // 2

    # Priors
    projection_parameters = sample(
        "projection_parameters",
        dist.Normal(
            jnp.zeros((num_projection_parameters,)),
            jnp.ones((num_projection_parameters,)),
        ),
    )

    # Project inputs
    w = householder_reparameterization(projection_parameters, n, m)
    projected_training_data = Dataset(x=training_data.x @ w, y=training_data.y)
    bgp_model(projected_training_data, use_alternate_priors=use_alternate_priors)


######################
# Prediction routine #
######################

# Body of the loop over the posterior draws (`posterior_draw` is one element)
def bfs_predict_inner(
    pred_x: np.ndarray,
    training_data: Dict[str, np.ndarray],
    dim_feature_space: int,
    posterior_draw: Dict[str, np.ndarray],
) -> Tuple[np.ndarray, np.ndarray]:
    # Problem dimensions
    m = dim_feature_space
    n = training_data["x"].shape[1]

    # Retrieve projection matrix
    projection_parameters = posterior_draw["projection_parameters"]
    w = householder_reparameterization(projection_parameters, n, m)

    # Project predictions sites
    projected_pred_x = pred_x @ w

    projected_training_data = {"x": training_data["x"] @ w, "y": training_data["y"]}

    return bgp_predict_loop_1(projected_pred_x, projected_training_data, posterior_draw)


@partial(jit, static_argnums=(3,))
def bfs_predict(
    pred_x: np.ndarray,
    posterior_draws: Dict[str, np.ndarray],
    training_data: Dict[str, np.ndarray],
    dim_feature_space: int,
) -> Tuple[np.ndarray, np.ndarray]:
    means, variances = jmap(
        partial(bfs_predict_inner, pred_x, training_data, dim_feature_space),
        posterior_draws,
    )
    return means.T, variances.T


######################################################
# High-Level Training/Prediction/Validation Routines #
######################################################


@dataclass(frozen=True)
class SfRidgeGpTrainingArtifacts(Struct):
    normalization_constants: NormalizationConstants
    posterior_draws: dict
    training_duration: float


def sf_ridge_gp_train(
    training_data: Dataset,
    mcmc_parameters: McmcParameters,
    dim_feature_space: int,
    mcmc_file_path: Path,
    use_alternate_priors: Optional[bool] = False,
) -> SfRidgeGpTrainingArtifacts:
    # Normalize training data
    (
        normalization_constants,
        normalized_training_data,
    ) = compute_and_apply_dataset_normalization(training_data)

    # Sample from the posterior chains using MCMC
    posterior_draws, training_duration = mcmc(
        bfs_model,
        (normalized_training_data, dim_feature_space),
        {"use_alternate_priors": use_alternate_priors},
        mcmc_parameters,
        mcmc_file_path,
    )

    return SfRidgeGpTrainingArtifacts(
        normalization_constants=normalization_constants,
        posterior_draws=posterior_draws,
        training_duration=training_duration,
    )


def sf_ridge_gp_predict(
    pred_x: np.ndarray,
    training_data: Dataset,
    training_artifacts: SfRidgeGpTrainingArtifacts,
) -> Tuple[np.ndarray, np.ndarray]:
    # Retrieve quantities of interest
    posterior_draws = training_artifacts.posterior_draws
    normalization_constants = training_artifacts.normalization_constants

    # Normalize the training data and the prediction sites
    norm_training_data = apply_dataset_normalization(
        training_data, normalization_constants
    )
    norm_pred_x = normalize(
        pred_x,
        normalization_constants.x_offset,
        normalization_constants.x_scaling,
    )

    # Problem dimensions
    num_predictions = pred_x.shape[0]
    dim_feature_space = posterior_draws["length_scales"].shape[1]
    num_posterior_draws = posterior_draws["length_scales"].shape[0]

    norm_means, norm_variances = bfs_predict(
        norm_pred_x,
        posterior_draws,
        {"x": norm_training_data.x, "y": norm_training_data.y},
        dim_feature_space,
    )

    # Denormalization and flattening of all stochastic dimensions
    means = denormalize(
        norm_means,
        normalization_constants.y_offset,
        normalization_constants.y_scaling,
    ).reshape((num_predictions, num_posterior_draws))

    variances = (norm_variances * normalization_constants.y_scaling ** 2).reshape(
        (num_predictions, num_posterior_draws)
    )

    return means, variances
