from dataclasses import dataclass
from enum import Enum
from functools import partial
from pathlib import Path
from typing import Dict, Tuple

from jax import numpy as jnp, jit, vmap
from jax._src.scipy.linalg import cho_factor, cho_solve, cholesky, solve_triangular
from jax.lax import map as jmap
import numpy as np
from numpyro import enable_x64, sample, deterministic, distributions as dist

from exp4.utils import Dataset, Struct

from .multifidelity_deep_gp import (
    mf_deep_gp_model,
    dmf_gp_lf_predict_loop_1,
    dmf_gp_hf_predict_loop_1,
    lf_kernel,
    hf_kernel,
)
from .utils.normalization import (
    NormalizationConstants,
    normalize,
    denormalize,
    compute_and_apply_mf_dataset_normalization,
    apply_mf_dataset_normalization,
)
from .utils.householder import householder_reparameterization
from .utils.mcmc import McmcParameters, mcmc
from .utils.linalg import subspace_angles

enable_x64()

############################################################
# Probabilistic Model for the Ridge Version of the Deep GP #
############################################################


class FsRelation(str, Enum):
    SHARED = "shared"
    DISTINCT = "distinct"
    RELATED = "related"


def mf_deep_ridge_gp_model(
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    feature_space_relation: str,
    dim_feature_space: int,
    jitter: float = 1e-6,
    use_alternate_priors: bool = False,
) -> None:
    # Problem dimensions
    m = dim_feature_space
    n = lf_training_data.x.shape[1]
    num_projection_parameters = m * n - m * (m - 1) // 2

    if feature_space_relation == "shared":
        # If feature spaces are shared, we only need one set of projection parameters

        # Priors
        projection_parameters = sample(
            "projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Compute projection matrix
        w_lf = householder_reparameterization(projection_parameters, n, m)
        w_hf = w_lf

    else:
        # If feature spaces are different or related, we need two sets of projection
        # parameters

        # Priors
        lf_projection_parameters = sample(
            "lf_projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Priors
        hf_projection_parameters = sample(
            "hf_projection_parameters",
            dist.Normal(
                jnp.zeros((num_projection_parameters,)),
                jnp.ones((num_projection_parameters,)),
            ),
        )

        # Compute different projection matrices for LF and HF
        w_lf = householder_reparameterization(lf_projection_parameters, n, m)
        w_hf = householder_reparameterization(hf_projection_parameters, n, m)

        if feature_space_relation == "related":
            # If the feature spaces are related, there is a prior joint distribution of
            # the two projection matrices with their subspace angle
            angles = deterministic("subspace_angles", subspace_angles(w_lf, w_hf))
            sample(
                "subspace_angles_obs",
                dist.HalfNormal(
                    0.4 * jnp.ones((dim_feature_space,)),
                ),
                obs=angles,
            )

    # Project inputs
    proj_lf_training_data = Dataset(x=lf_training_data.x @ w_lf, y=lf_training_data.y)
    proj_hf_training_data = Dataset(x=hf_training_data.x @ w_hf, y=hf_training_data.y)

    return mf_deep_gp_model(
        proj_lf_training_data,
        proj_hf_training_data,
        jitter=jitter,
        use_alternate_priors=use_alternate_priors,
    )


##############################################################
# Prediction Functions for the Low- and High-Fidelity Models #
##############################################################

# Body of the loop over the posterior draws (`posterior_draw` is one element)
def dmf_ridge_gp_lf_predict_loop_1(
    norm_pred_x: np.ndarray,
    lf_training_data: Dict[str, np.ndarray],
    dim_feature_space: int,
    feature_space_relation: FsRelation,
    posterior_draw: Dict[str, np.ndarray],
    jitter=1e-8,
):
    # Problem dimensions
    dim_inputs = norm_pred_x.shape[1]

    # Retrieve projection matrix
    projection_parameters = (
        posterior_draw["projection_parameters"]
        if feature_space_relation == "shared"
        else posterior_draw["lf_projection_parameters"]
    )
    w_lf = householder_reparameterization(
        projection_parameters, dim_inputs, dim_feature_space
    )

    # Project prediction sites
    proj_pred_x = norm_pred_x @ w_lf

    # Project LF training data
    proj_lf_training_data = {
        "x": lf_training_data["x"] @ w_lf,
        "y": lf_training_data["y"],
    }

    return dmf_gp_lf_predict_loop_1(
        proj_pred_x, posterior_draw, proj_lf_training_data, jitter=jitter
    )


@partial(jit, static_argnums=(3, 4))
def lf_predict(
    norm_pred_x: np.ndarray,
    posterior_draws: Dict[str, np.ndarray],
    norm_lf_training_data: Dict[str, np.ndarray],
    dim_feature_space: int,
    feature_space_relation: FsRelation,
    jitter=1e-8,
):
    # The most outer loop in the prediction routine uses jmap instead of vmap to reduce
    # the memory footprint
    norm_lf_means, norm_lf_variances = jmap(
        partial(
            dmf_ridge_gp_lf_predict_loop_1,
            norm_pred_x,
            norm_lf_training_data,
            dim_feature_space,
            feature_space_relation,
            jitter=jitter,
        ),
        posterior_draws,
    )
    return norm_lf_means.T, norm_lf_variances.T


# # Body of the loop over the posterior draws (`posterior_draw` is one element)
# # Note: `norm_lf_y_samples` is now size (num_predictions x num_lf_samples)
# def dmf_ridge_gp_hf_predict_loop_1(
#     norm_pred_x: np.ndarray,
#     norm_hf_training_data: Dict[str, np.ndarray],
#     dim_feature_space: int,
#     feature_space_relation: FsRelation,
#     norm_lf_y_samples_and_posterior_draw: Tuple[np.ndarray, Dict[str, np.ndarray]],
#     jitter: float = 1e-8,
# ):
#     # Extract `norm_lf_y_samples` and `posterior_draw`
#     norm_lf_y_samples = norm_lf_y_samples_and_posterior_draw[0]
#     posterior_draw = norm_lf_y_samples_and_posterior_draw[1]

#     # Problem dimensions
#     dim_inputs = norm_pred_x.shape[1]

#     # Retrieve projection matrix
#     projection_parameters = (
#         posterior_draw["projection_parameters"]
#         if feature_space_relation == "shared"
#         else posterior_draw["hf_projection_parameters"]
#     )
#     w_hf = householder_reparameterization(
#         projection_parameters, dim_inputs, dim_feature_space
#     )

#     # Project prediction sites
#     proj_pred_x = norm_pred_x @ w_hf

#     # Project LF training data
#     proj_hf_training_data = {
#         "x": norm_hf_training_data["x"] @ w_hf,
#         "y": norm_hf_training_data["y"],
#     }

#     return dmf_gp_hf_predict_loop_1(
#         proj_pred_x,
#         norm_lf_y_samples,
#         posterior_draw,
#         proj_hf_training_data,
#         jitter=jitter,
#     )


# @partial(jit, static_argnums=(4, 5))
# def hf_predict(
#     norm_pred_x: np.ndarray,
#     posterior_draws: Dict[str, np.ndarray],
#     norm_hf_training_data: Dict[str, np.ndarray],
#     norm_lf_y_samples: np.ndarray,
#     dim_feature_space: int,
#     feature_space_relation: FsRelation,
#     jitter=1e-8,
# ):
#     # The most outer loop in the prediction routine uses jmap instead of vmap to reduce
#     # the memory footprint
#     norm_hf_means, norm_hf_variances = jmap(
#         partial(
#             dmf_ridge_gp_hf_predict_loop_1,
#             norm_pred_x,
#             norm_hf_training_data,
#             dim_feature_space,
#             feature_space_relation,
#             jitter=jitter,
#         ),
#         (norm_lf_y_samples, posterior_draws),
#     )
#     return jnp.transpose(norm_hf_means, (1, 0, 2)), jnp.transpose(
#         norm_hf_variances, (1, 0, 2)
#     )
#     # originally num_posterior_draws x num_predictions x num_lf_predictions
#     # transposed to num_predictions x num_posterior_draws x num_lf_predictions


#####################################################
# Alternate Prediction Function (KF and HF at once) #
#####################################################


def lf_gp_pred_mean_cov(
    pred_x: np.ndarray,
    train_x: np.ndarray,
    train_y: np.ndarray,
    model_parameters: Dict[str, np.ndarray],
) -> Tuple[np.ndarray, np.ndarray]:
    """Rasmussen and Williams equations 2.23 and 2.24 p. 16"""

    k_xs_x = lf_kernel(pred_x, train_x, model_parameters)
    k_xs_xs = lf_kernel(pred_x, pred_x, model_parameters)
    k_x_x = lf_kernel(train_x, train_x, model_parameters)
    I_n = jnp.eye(train_x.shape[0])
    cho_k_x_x = cho_factor(
        k_x_x + model_parameters["lf_noise_variance"] * I_n, lower=True
    )
    means = (k_xs_x @ cho_solve(cho_k_x_x, train_y)).reshape(-1)
    covariances = k_xs_xs + k_xs_x @ cho_solve(cho_k_x_x, k_xs_x.T)

    return means, covariances


# def hf_gp_pred_mean_cov(
#     pred_x: np.ndarray,
#     train_x: np.ndarray,
#     train_y: np.ndarray,
#     model_parameters: Dict[str, np.ndarray],
#     jitter=1e-10,
# ) -> Tuple[np.ndarray, np.ndarray]:
#     """Rasmussen and Williams equations 2.23 and 2.24 p. 16"""
#     num_training_samples = train_x.shape[0]
#     k_xs_x = hf_kernel(pred_x, train_x, model_parameters)
#     k_xs_xs = hf_kernel(pred_x, pred_x, model_parameters)
#     k_x_x = (
#         hf_kernel(train_x, train_x, model_parameters)
#         + model_parameters["hf_noise_variance"] * jnp.eye(num_training_samples)
#         + jitter * jnp.eye(num_training_samples)
#     )
#     cho_k_x_x = cho_factor(k_x_x)
#     means = (k_xs_x @ cho_solve(cho_k_x_x, train_y)).reshape(-1)
#     covariances = k_xs_xs + k_xs_x @ cho_solve(cho_k_x_x, k_xs_x.T)

#     return means, covariances


def hf_gp_pred_mean_var_loop_1(
    train_x: np.ndarray,
    c_and_lower: Tuple[np.ndarray, bool],
    alpha: np.ndarray,
    model_parameters: Dict[str, np.ndarray],
    pred_x: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    # Mean
    pred_x = pred_x.reshape((1, -1))
    k_star = hf_kernel(train_x, pred_x, model_parameters)
    mean = (k_star.T @ alpha)[0, 0]

    # Variance
    v = solve_triangular(c_and_lower[0], k_star, lower=c_and_lower[1])
    k_xs_xs = hf_kernel(pred_x, pred_x, model_parameters)
    variance = (k_xs_xs - v.T @ v)[0, 0]

    return mean, variance


def hf_gp_pred_mean_var(
    pred_x: np.ndarray,
    train_x: np.ndarray,
    train_y: np.ndarray,
    model_parameters: Dict[str, np.ndarray],
    jitter: float = 1e-8,
) -> Tuple[np.ndarray, np.ndarray]:
    I_n = jnp.eye(train_x.shape[0])
    k_x_x = (
        hf_kernel(train_x, train_x, model_parameters)
        + model_parameters["hf_noise_variance"] * I_n
        + jitter * I_n
    )
    c_and_lower = cho_factor(k_x_x, lower=True)
    alpha = cho_solve(c_and_lower, train_y)

    # Loop over the prediction sites
    return jmap(
        partial(
            hf_gp_pred_mean_var_loop_1, train_x, c_and_lower, alpha, model_parameters
        ),
        pred_x,
    )

    # return vmap(hf_gp_pred_mean_var_loop_1, in_axes=(None, None, None, None, 0))(
    #     train_x, c_and_lower, alpha, model_parameters, pred_x
    # )


# Body of the loop over the LF samples
def predict_loop_2(
    pred_x: np.ndarray,
    hf_training_samples: Dict[str, np.ndarray],
    posterior_draw: Dict[str, np.ndarray],
    proj_mat: np.ndarray,
    lf_draw: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    # Problem dimensions
    num_hf_train = hf_training_samples["x"].shape[0]

    # Concat. (axis 1) projected actual HF train sites and LF outputs for HF train sites
    lf_hf_train_draw = lf_draw[:num_hf_train].reshape((-1, 1))
    concat_proj_train_x = jnp.concatenate(
        (hf_training_samples["x"] @ proj_mat, lf_hf_train_draw), axis=1
    )

    # Concatenate (axis 1) projected actual pred sites and LF output for HF pred sites
    lf_pred_draw = lf_draw[num_hf_train:].reshape((-1, 1))
    concat_proj_pred_x = jnp.concatenate((pred_x @ proj_mat, lf_pred_draw), axis=1)

    # Compute mean and variance-covariance matrix for the HF outputs
    # hf_means, hf_covariances = hf_gp_pred_mean_cov(
    #     concat_proj_pred_x,
    #     concat_proj_train_x,
    #     hf_training_samples["y"],
    #     posterior_draw,
    # )

    # return hf_means, hf_covariances

    hf_means, hf_variances = hf_gp_pred_mean_var(
        concat_proj_pred_x,
        concat_proj_train_x,
        hf_training_samples["y"],
        posterior_draw,
    )

    return hf_means, hf_variances


# Body of the loop over the posterior draws
def predict_loop_1(
    pred_x: np.ndarray,
    lf_train_samples: Dict[str, np.ndarray],
    hf_train_samples: Dict[str, np.ndarray],
    std_gaussian_draws: np.ndarray,
    dim_feature_space: int,
    posterior_draw: Dict[str, np.ndarray],
    # jitter=1e-8,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Parameters

    - pred_x is size num_predictions x dim_inputs
    - lf_training_samples is a Dict
      - x is size num_lf_training_samples x dim_inputs
      - y is size num_lf_training_samples x 1
    - hf_training_samples is a Dict
      - x is size num_hf_training_samples x dim_inputs
      - y is size num_hf_training_samples x 1
    - std_gaussian_draws is size
      num_intermediate_lf_draws x (num_hf_training_samples + num_predictions)
    - posterior_draws is a Dict of np.ndarrays with shape[0] = 1
    """
    # Problem size
    num_hf_train = hf_train_samples["x"].shape[0]
    dim_inputs = lf_train_samples["x"].shape[1]

    # Pre-compute the projection matrix
    # ⚠ this assumes FS is shared between LF and HF
    proj_mat = householder_reparameterization(
        posterior_draw["projection_parameters"], dim_inputs, dim_feature_space
    )

    # Concatenate (axis 0) and project HF training and prediction sites
    # Yields a (num_hf_training samples + num_predictions) x dim_feature_space matrix
    concat_proj_pred_x = (
        jnp.concatenate((hf_train_samples["x"], pred_x), axis=0) @ proj_mat
    )

    # Project LF training samples
    proj_train_x = lf_train_samples["x"] @ proj_mat

    # Compute the mean vector and variance-covariance matrix
    # Mean is size (num_hf_training samples + num_predictions)
    # Covariance is a square matrix of size (num_hf_training samples + num_predictions)
    all_lf_means, all_lf_covariances = lf_gp_pred_mean_cov(
        concat_proj_pred_x, proj_train_x, lf_train_samples["y"], posterior_draw
    )

    # Artificially draw samples using pre-drawn Gaussian samples
    # Yields a matrix of size
    # num_intermediate_lf_draws x (num_hf_training_samples + num_predictions)
    cho_all_lf_covariances = cholesky(
        all_lf_covariances + 1e-8 * jnp.eye(all_lf_covariances.shape[0]), lower=True
    )
    all_lf_draws = all_lf_means + std_gaussian_draws @ cho_all_lf_covariances.T

    # Separate pred means and covariances at LF training sites from the rest
    lf_pred_means = all_lf_means[num_hf_train:]
    lf_pred_variances = all_lf_covariances[num_hf_train:, num_hf_train:].diagonal()

    # Iterate over the LF samples to get pred means and covariances at HF training sites
    hf_pred_means, hf_pred_variances = jmap(
        partial(
            predict_loop_2,
            pred_x,
            hf_train_samples,
            posterior_draw,
            proj_mat,
        ),
        all_lf_draws,
    )
    # hf_pred_means, hf_pred_covariances = vmap(
    #     predict_loop_2, in_axes=(None, None, None, None, 0)
    # )(pred_x, hf_train_samples, posterior_draw, proj_mat, all_lf_draws)

    return lf_pred_means, lf_pred_variances, hf_pred_means, hf_pred_variances


@partial(jit, static_argnames=("dim_feature_space",))
def predict(
    pred_x: np.ndarray,
    lf_training_samples: Dict[str, np.ndarray],
    hf_training_samples: Dict[str, np.ndarray],
    lf_std_gaussian_draws: np.ndarray,
    dim_feature_space: int,
    posterior_draws: Dict[str, np.ndarray],
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Returns predictive means and covariances for the predicted points. Their
    respective shapes are:
    - lf_pred_means: num_posterior_draws x num_predictions
    - lf_pred_covariances: num_posterior_draws x num_predictions x num_predictions
    - hf_pred_means: num_posterior_draws x num_intermediate_lf_draws x num_predictions
    - hf_pred_covariances:
      num_posterior_draws
        x num_intermediate_lf_draws
        x num_predictions
        x num_predictions
    """

    # ⚠ TEST Thinning

    # # Posterior draws
    # new_posterior_draws = {}
    # for key, value in posterior_draws.items():
    #     new_posterior_draws[key] = value[::10]
    # posterior_draws = new_posterior_draws

    # # Intermediate LF samples
    # lf_std_gaussian_draws = lf_std_gaussian_draws[::2]

    # ⚠ END TEST Thinning

    # Iterate over the posterior draws
    return vmap(predict_loop_1, in_axes=(None, None, None, None, None, 0))(
        pred_x,
        lf_training_samples,
        hf_training_samples,
        lf_std_gaussian_draws,
        dim_feature_space,
        posterior_draws,
    )
    # return jmap(
    #     partial(
    #         predict_loop_1,
    #         pred_x,
    #         lf_training_samples,
    #         hf_training_samples,
    #         lf_std_gaussian_draws,
    #         dim_feature_space,
    #     ),
    #     posterior_draws,
    # )


####################
# Training Routine #
####################


@dataclass(frozen=True)
class DeepMfRidgeGpTrainingArtifacts(Struct):
    normalization_constants: NormalizationConstants
    posterior_draws: Dict[str, np.ndarray]
    training_duration: float


def mf_deep_ridge_gp_train(
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    mcmc_parameters: McmcParameters,
    feature_space_relation: FsRelation,
    dim_feature_space: int,
    mcmc_file_path: Path,
    use_alternate_priors: bool = False,
) -> DeepMfRidgeGpTrainingArtifacts:
    # Normalize training data
    (
        normalization_constants,
        norm_lf_training_data,
        norm_hf_training_data,
    ) = compute_and_apply_mf_dataset_normalization(lf_training_data, hf_training_data)

    # Sample from the posterior chains using MCMC
    posterior_draws, training_duration = mcmc(
        mf_deep_ridge_gp_model,
        (
            norm_lf_training_data,
            norm_hf_training_data,
            feature_space_relation,
            dim_feature_space,
        ),
        {"use_alternate_priors": use_alternate_priors},
        mcmc_parameters,
        mcmc_file_path,
    )

    return DeepMfRidgeGpTrainingArtifacts(
        normalization_constants=normalization_constants,
        posterior_draws=posterior_draws,
        training_duration=training_duration,
    )


######################
# Prediction Routine #
######################


# def mf_deep_ridge_gp_predict(
#     pred_x: np.ndarray,
#     lf_training_data: Dataset,
#     hf_training_data: Dataset,
#     training_artifacts: DeepMfRidgeGpTrainingArtifacts,
#     feature_space_relation: FsRelation,
#     dim_feature_space: int,
#     std_gaussian_samples: np.ndarray,
# ):
#     # Retrieve quantities of interest
#     posterior_draws = training_artifacts.posterior_draws
#     normalization_constants = training_artifacts.normalization_constants

#     # Normalize the training data and the prediction sites
#     (norm_lf_training_data, norm_hf_training_data,) = apply_mf_dataset_normalization(
#         lf_training_data, hf_training_data, normalization_constants
#     )
#     norm_pred_x = normalize(
#         pred_x,
#         normalization_constants.x_offset,
#         normalization_constants.x_scaling,
#     )

#     # Problem dimensions
#     num_predictions = pred_x.shape[0]

#     # LF GP
#     # We make predictions at both the training points and the desired prediction sites
#     # num_posterior_draws
#     #   x num_intermediate_lf_draws
#     #   x (num_hf_training_points + 1)
#     #   x num_predictions
#     norm_lf_means, norm_lf_variances = lf_predict(
#         norm_pred_x,
#         posterior_draws,
#         {"x": norm_lf_training_data.x, "y": norm_lf_training_data.y},
#         dim_feature_space,
#         feature_space_relation,
#     )

#     norm_lf_samples = (
#         norm_lf_means[None, ...]
#         + std_gaussian_samples[..., None, None] * jnp.sqrt(norm_lf_variances)[None, ...]
#     ).transpose((2, 1, 0))
#     # originally num_lf_predictions x num_predictions x num_posterior_draws
#     # transposed to num_posterior_draws x num_predictions x num_lf_predictions
#     # as expected by hf_predict

#     # Before making predictions fully deterministic
#     # norm_lf_samples = (
#     #     dist.Normal(loc=norm_lf_means, scale=jnp.sqrt(norm_lf_variances))
#     #     .sample(PRNGKey(lf_predictions_seed), sample_shape=(num_lf_predictions,))
#     #     .transpose((2, 1, 0))
#     #     # originally num_lf_predictions x num_predictions x num_posterior_draws
#     #     # transposed to num_posterior_draws x num_predictions x num_lf_predictions
#     # )

#     # Second GP
#     norm_hf_means, norm_hf_variances = hf_predict(
#         norm_pred_x,
#         posterior_draws,
#         {"x": norm_hf_training_data.x, "y": norm_hf_training_data.y},
#         norm_lf_samples,
#         dim_feature_space,
#         feature_space_relation,
#     )

#     # Denormalization and flattening of all stochastic dimensions ##################

#     lf_means = denormalize(
#         norm_lf_means,
#         normalization_constants.y_offset,
#         normalization_constants.y_scaling,
#     ).reshape((num_predictions, -1))

#     lf_variances = (norm_lf_variances * normalization_constants.y_scaling ** 2).reshape(
#         (num_predictions, -1)
#     )

#     hf_means = denormalize(
#         norm_hf_means,
#         normalization_constants.y_offset,
#         normalization_constants.y_scaling,
#     ).reshape((num_predictions, -1))

#     hf_variances = (norm_hf_variances * normalization_constants.y_scaling ** 2).reshape(
#         (num_predictions, -1)
#     )

#     return lf_means, lf_variances, hf_means, hf_variances


def mf_deep_ridge_gp_predict(
    pred_x: np.ndarray,
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    training_artifacts: DeepMfRidgeGpTrainingArtifacts,
    dim_feature_space: int,
    std_gaussian_samples: np.ndarray,
):
    # Retrieve quantities of interest
    posterior_draws = training_artifacts.posterior_draws
    normalization_constants = training_artifacts.normalization_constants

    # Normalize the training data and the prediction sites
    (norm_lf_training_data, norm_hf_training_data,) = apply_mf_dataset_normalization(
        lf_training_data, hf_training_data, normalization_constants
    )
    norm_pred_x = normalize(
        pred_x,
        normalization_constants.x_offset,
        normalization_constants.x_scaling,
    )

    # Prediction
    (
        norm_lf_pred_means,
        norm_lf_pred_variances,
        norm_hf_pred_means,
        norm_hf_pred_variances,
    ) = predict(
        norm_pred_x,
        {"x": norm_lf_training_data.x, "y": norm_lf_training_data.y},
        {"x": norm_hf_training_data.x, "y": norm_hf_training_data.y},
        std_gaussian_samples,
        dim_feature_space,
        posterior_draws,
    )

    # norm_hf_pred_variances = norm_hf_pred_variances.diagonal(axis1=2, axis2=3)

    # Flattening of all stochastic dimensions ##########################################
    num_predictions = pred_x.shape[0]

    # Originally num_posteriors_draws x num_predictions
    # Simply transposed
    norm_lf_pred_means = norm_lf_pred_means.T
    norm_lf_pred_variances = norm_lf_pred_variances.T

    # Originally num_posteriors_draws x num_intermediate_lf_draws x num_predictions
    norm_hf_pred_means = norm_hf_pred_means.transpose(2, 0, 1).reshape(
        num_predictions, -1
    )
    norm_hf_pred_variances = norm_hf_pred_variances.transpose(2, 0, 1).reshape(
        num_predictions, -1
    )

    # Denormalization ##################################################################

    offset = normalization_constants.y_offset
    scaling = normalization_constants.y_scaling

    lf_means = denormalize(norm_lf_pred_means, offset, scaling)
    lf_variances = norm_lf_pred_variances * scaling ** 2
    hf_means = denormalize(norm_hf_pred_means, offset, scaling)
    hf_variances = norm_hf_pred_variances * scaling ** 2

    return lf_means, lf_variances, hf_means, hf_variances
