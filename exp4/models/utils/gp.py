from jax import numpy as jnp

######
# GP #
######


def squared_distance(x1, x2):
    return (
        jnp.sum(x1 ** 2, axis=1, keepdims=True)
        - 2.0 * x1 @ x2.T
        + jnp.sum(x2 ** 2, axis=1, keepdims=True).T
    )


def se_kernel(x1, x2, signal_variance, length_scales):
    return signal_variance * jnp.exp(
        -0.5 * squared_distance(x1 / length_scales, x2 / length_scales)
    )
