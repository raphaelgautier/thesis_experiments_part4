from dataclasses import dataclass

import numpy as np
from typing import Tuple

from exp4.utils import Dataset, Struct

#################
# Normalization #
#################


@dataclass(frozen=True)
class NormalizationConstants(Struct):
    x_offset: float
    x_scaling: float
    y_offset: float
    y_scaling: float


def compute_normalization(x: np.ndarray) -> Tuple[float, float]:
    """Normalize vector `x` using its mean and standard deviation."""
    return np.mean(x), np.std(x)


def normalize(x: np.ndarray, offset: float, scaling: float) -> np.ndarray:
    return (x - offset) / scaling


def denormalize(x: np.ndarray, offset: float, scaling: float) -> np.ndarray:
    return offset + x * scaling


def apply_dataset_normalization(
    training_data: Dataset, nc: NormalizationConstants
) -> Dataset:
    return Dataset(
        x=normalize(training_data.x, nc.x_offset, nc.x_scaling),
        y=normalize(training_data.y, nc.y_offset, nc.y_scaling),
    )


def apply_mf_dataset_normalization(
    lf_training_data: Dataset, hf_training_data: Dataset, nc: NormalizationConstants
):
    norm_lf_training_data = apply_dataset_normalization(lf_training_data, nc)
    norm_hf_training_data = apply_dataset_normalization(hf_training_data, nc)
    return norm_lf_training_data, norm_hf_training_data


def compute_dataset_normalization(training_data: Dataset) -> NormalizationConstants:
    # Normalize inputs
    x_offset, x_scaling = compute_normalization(training_data.x)

    # Normalize outputs
    y_offset, y_scaling = compute_normalization(training_data.y)

    # Package normalization constants
    return NormalizationConstants(
        x_offset=x_offset, x_scaling=x_scaling, y_offset=y_offset, y_scaling=y_scaling
    )


def compute_and_apply_dataset_normalization(
    training_data: Dataset,
) -> Tuple[NormalizationConstants, Dataset]:
    nc = compute_dataset_normalization(training_data)
    norm_training_data = apply_dataset_normalization(training_data, nc)
    return nc, norm_training_data


def compute_mf_dataset_normalization(
    lf_training_data: Dataset, hf_training_data: Dataset
) -> NormalizationConstants:
    # We compute normalization constants using all data
    training_data = Dataset(
        x=np.concatenate((lf_training_data.x, hf_training_data.x), 0),
        y=np.concatenate((lf_training_data.y, hf_training_data.y), 0),
    )
    return compute_dataset_normalization(training_data)


def compute_and_apply_mf_dataset_normalization(
    lf_training_data: Dataset, hf_training_data: Dataset
) -> Tuple[NormalizationConstants, Dataset, Dataset]:
    nc = compute_mf_dataset_normalization(lf_training_data, hf_training_data)
    norm_lf_training_data, norm_hf_training_data = apply_mf_dataset_normalization(
        lf_training_data, hf_training_data, nc
    )
    return nc, norm_lf_training_data, norm_hf_training_data
