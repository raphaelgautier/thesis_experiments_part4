from jax import numpy as jnp
from jax.ops import index, index_update

#################################
# Householder Reparametrization #
#################################


def householder(v, n):
    v = v[:, None]
    k = v.shape[0]
    sgn = jnp.sign(v[0])
    u = v + sgn * jnp.linalg.norm(v) * jnp.eye(k, 1)
    u = u / jnp.linalg.norm(u)
    h_k = -sgn * (jnp.eye(k) - 2 * u @ u.T)
    h_k_hat = jnp.eye(n)
    start = n - k
    h_k_hat = index_update(h_k_hat, index[start:, start:], h_k)
    return h_k_hat


def householder_reparameterization(
    projection_parameters, num_original_inputs, num_active_dims
):
    m, n = num_active_dims, num_original_inputs
    h = jnp.eye(n)
    i_end = 0
    for i in range(m):
        i_start = i_end
        i_end = i_start + n - i
        h_k_hat = householder(projection_parameters[i_start:i_end], n)
        h = h_k_hat @ h
    return h[:, :m]
