from jax import numpy as jnp
from jax.scipy.linalg import svd
import numpy as np
from scipy.stats.distributions import norm

##########################################################
# Find an Orthogonal Basis for the Orthogonal Complement #
##########################################################


def construct_w_orth(w):
    # Problem parameters
    dim_inputs = w.shape[0]
    dim_fs = w.shape[1]
    dim_orth_fs = dim_inputs - dim_fs

    mat = np.concatenate((w, norm().rvs(size=(dim_inputs, dim_orth_fs))), axis=1)
    for i in range(dim_fs, dim_inputs):
        mat[:, i] -= np.sum(mat[:, :i] * (mat[:, i, None].T @ mat[:, :i]), axis=1)
        mat[:, i] /= np.linalg.norm(mat[:, i])
    return mat[:, dim_fs:]


###################
# Subspace Angles #
###################


def subspace_angles(W1, W2):
    # Computes the subspace angles between **two orthogonal matrices**.
    # ⚠ this is not valid for non-orthogonal matrices
    return jnp.arccos(svd(W1.T @ W2, compute_uv=False))
