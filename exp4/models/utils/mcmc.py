from dataclasses import dataclass
from enum import Enum
from pathlib import Path
import time
from typing import Callable, Dict, Tuple

import dill
from jax import numpy as jnp
from jax.random import PRNGKey
from numpyro.diagnostics import print_summary
from numpyro.infer.util import initialize_model
from numpyro.infer.hmc import hmc
from numpyro.util import fori_collect

from exp4.utils import Struct

########
# MCMC #
########


@dataclass(frozen=True)
class McmcParameters(Struct):
    target_acceptance_probability: float = 0.8
    num_warmup_draws: int = 500
    num_posterior_draws: int = 1000
    random_seed: int = 0
    num_draws_between_saves: int = 10
    progress_bar: bool = False
    display_summary: bool = False


class MCMCState(str, Enum):
    WARMUP = 1
    SAMPLING = 2


def mcmc(
    numpyro_model: Callable,
    model_args: Tuple,
    model_kwargs: Dict,
    mcmc_parameters: McmcParameters,
    mcmc_file_path: Path,
) -> Tuple[Dict, float]:
    # Extract parameters
    target_acceptance_probability = mcmc_parameters.target_acceptance_probability
    num_warmup_draws = mcmc_parameters.num_warmup_draws
    num_posterior_draws = mcmc_parameters.num_posterior_draws
    num_draws_between_saves = mcmc_parameters.num_draws_between_saves
    random_seed = mcmc_parameters.random_seed
    progress_bar = mcmc_parameters.progress_bar
    display_summary = mcmc_parameters.display_summary

    # Does `mcmc_file_path` exist?
    if mcmc_file_path is not None and mcmc_file_path.is_file():
        # If it does, we use it to retrieve the latest MCMC state
        with open(mcmc_file_path, "rb") as mcmc_file:
            saved_state = dill.load(mcmc_file)
        model_info = saved_state["model_info"]
        hmc_state = saved_state["hmc_state"]
        remaining_warmup_draws = saved_state["remaining_warmup_draws"]
        remaining_sampling_draws = saved_state["remaining_sampling_draws"]
        training_duration = saved_state["training_duration"]
        posterior_draws = saved_state["posterior_draws"]

        init_kernel, sample_kernel = hmc(model_info.potential_fn, algo="NUTS")
        _ = init_kernel(
            model_info.param_info,
            num_warmup=remaining_warmup_draws,
            target_accept_prob=target_acceptance_probability,
        )

    else:
        # Otherwise, we initialize a new state

        # Manual MCMC initialization
        model_info = initialize_model(
            PRNGKey(random_seed),
            numpyro_model,
            model_args=model_args,
            model_kwargs=model_kwargs,
        )
        init_kernel, sample_kernel = hmc(model_info.potential_fn, algo="NUTS")
        hmc_state = init_kernel(
            model_info.param_info,
            num_warmup=num_warmup_draws,
            target_accept_prob=target_acceptance_probability,
        )

        # Counters and draws initializations
        remaining_warmup_draws = num_warmup_draws
        remaining_sampling_draws = num_posterior_draws
        training_duration = 0
        posterior_draws = None

    # We keep sampling until we are done
    while remaining_sampling_draws > 0:

        if remaining_warmup_draws > 0:
            # Warmup
            state = MCMCState.WARMUP
            upper = min(remaining_warmup_draws, num_draws_between_saves)
        else:
            state = MCMCState.SAMPLING
            upper = min(remaining_sampling_draws, num_draws_between_saves)

        start_time = time.time()

        new_draws, hmc_state = fori_collect(
            0,
            upper,
            sample_kernel,
            hmc_state,
            progbar=progress_bar,
            return_last_val=True,
            transform=lambda state: model_info.postprocess_fn(state.z),
        )

        training_duration += time.time() - start_time

        # Subtract number of draws, update posterior draws if applicable
        if state == MCMCState.WARMUP:
            remaining_warmup_draws -= upper
        elif state == MCMCState.SAMPLING:
            remaining_sampling_draws -= upper
            posterior_draws = (
                new_draws
                if posterior_draws is None
                else {
                    key: jnp.concatenate((posterior_draws[key], new_draws[key]))
                    for key in new_draws.keys()
                }
            )

        # Save progress
        with open(mcmc_file_path, "wb") as mcmc_file:
            dill.dump(
                {
                    "model_info": model_info,
                    "hmc_state": hmc_state,
                    "training_duration": training_duration,
                    "remaining_warmup_draws": remaining_warmup_draws,
                    "remaining_sampling_draws": remaining_sampling_draws,
                    "posterior_draws": posterior_draws,
                },
                mcmc_file,
            )

    if display_summary:
        assert posterior_draws is not None
        print_summary(
            {key: value[None, ...] for key, value in posterior_draws.items()}, prob=0.9
        )

    # Making sure we are returning the right type
    assert type(posterior_draws) is dict

    return posterior_draws, training_duration
