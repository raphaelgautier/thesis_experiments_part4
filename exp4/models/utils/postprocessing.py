from jax import numpy as jnp

##################################
# Post-Processing of MCMC chains #
##################################


def process_chains(
    posterior_draws, grouped_by_chain, ungroup=False, num_thinned_draws=None
):
    # Alternate dictionary holding the newly processed chains
    processed_draws = {}

    for site_name, site_draws in posterior_draws.items():
        # Whether the current site draws is grouped by chain
        site_draws_grouped_by_chain = grouped_by_chain

        # Flatten if necessary
        if site_draws_grouped_by_chain and ungroup:
            processed_draws[site_name] = jnp.reshape(
                site_draws, (-1,) + site_draws.shape[2:]
            )
            site_draws_grouped_by_chain = False
        else:
            processed_draws[site_name] = site_draws

        # Thin if necessary
        if num_thinned_draws is not None:
            chain_length = (
                processed_draws[site_name].shape[1]
                if site_draws_grouped_by_chain
                else processed_draws[site_name].shape[0]
            )
            if num_thinned_draws < chain_length:
                thinning_factor = chain_length // num_thinned_draws
                last_index = thinning_factor * num_thinned_draws

                if site_draws_grouped_by_chain:
                    processed_draws[site_name] = processed_draws[site_name][
                        :, :last_index:thinning_factor
                    ]
                else:
                    processed_draws[site_name] = processed_draws[site_name][
                        :last_index:thinning_factor
                    ]

    return processed_draws
