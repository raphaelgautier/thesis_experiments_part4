from dataclasses import dataclass, fields
import enum
from typing import TypeVar, Type, Dict
from typing_extensions import get_origin

from jax import xla
import h5py as h5
import numpy as np


@dataclass(frozen=True)
class Struct:
    pass


#######################
# Persistence Helpers #
#######################


def pydict_to_h5group(pydict: Dict, h5group: h5.Group) -> None:
    for key, value in pydict.items():
        # ignore value if it is None
        if value is not None:
            if isinstance(value, dict):
                subgroup = h5group.create_group(str(key))
                pydict_to_h5group(value, subgroup)
            else:
                # Conversions for certain types
                if isinstance(value, xla.DeviceArray):
                    value = np.array(value)
                elif isinstance(value, enum.Enum):
                    value = value.value

                # Save
                if isinstance(value, np.ndarray):
                    if value.size == 1:
                        h5group.attrs[key] = value.flatten()[0]
                    else:
                        h5group.create_dataset(key, data=value)
                else:
                    h5group.attrs[key] = value


def h5group_to_pydict(h5group: h5.Group) -> Dict:
    d = dict()
    for key, value in h5group.items():
        if isinstance(value, h5.Group):
            d[key] = h5group_to_pydict(value)
        elif isinstance(value, h5.Dataset):
            d[key] = np.array(value)
        else:
            d[key] = value

    for key, value in h5group.attrs.items():
        d[key] = value

    return d


##################
# Struct Helpers #
##################


def struct_to_pydict(struct: Struct) -> Dict:
    new_dict = {}
    for field in fields(struct):
        field_value = getattr(struct, field.name)
        if field_value is None:
            # We skip None values
            pass
        elif get_origin(field.type) is not None:
            new_dict[field.name] = field_value
        elif issubclass(field.type, Struct):
            new_dict[field.name] = struct_to_pydict(field_value)
        elif issubclass(field.type, enum.Enum):
            if issubclass(field.type, str):
                new_dict[field.name] = field_value
            else:
                new_dict[field.name] = field_value.value
        else:
            new_dict[field.name] = field_value

    return new_dict


T = TypeVar("T", bound=Struct)


def pydict_to_struct(pydict: Dict, struct_cls: Type[T]) -> T:
    arg_dict = {}
    for field in fields(struct_cls):
        try:
            field_value = pydict[field.name]

            if get_origin(field.type) is not None:
                arg_dict[field.name] = field_value
                # args = get_args(field.type)
                # if len(args) == 2 and args[1] is None:
                #     arg_dict[field.name] = args[0](field_value)
            elif issubclass(field.type, Struct):
                arg_dict[field.name] = pydict_to_struct(field_value, field.type)
            elif issubclass(field.type, enum.Enum):
                arg_dict[field.name] = field.type(field_value)
            else:
                arg_dict[field.name] = field_value
        except KeyError:
            arg_dict[field.name] = None
    instance = struct_cls(**arg_dict)
    return instance


################
# Combinations #
################


def h5group_to_struct(h5group: h5.Group, struct_cls: Type[T]) -> T:
    return pydict_to_struct(h5group_to_pydict(h5group), struct_cls)


def struct_to_h5group(struct: Struct, h5group: h5.Group) -> None:
    pydict_to_h5group(struct_to_pydict(struct), h5group)
