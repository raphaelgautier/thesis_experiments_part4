from exp4.settings import LOG_FILE

from .time import compact_timestamp

###################
# Logging Utility #
###################


def log(message):
    LOG_FILE.touch()
    with open(LOG_FILE, "a") as file:
        file.write("{}: {}\n".format(compact_timestamp(), message))
