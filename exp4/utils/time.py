from datetime import datetime

####################
# Timestamp Helper #
####################


def compact_timestamp():
    return "{:%Y%m%d_%H%M%S}".format(datetime.now())
