from dataclasses import dataclass
import numpy as np

from .persistence import Struct


@dataclass(frozen=True)
class Dataset(Struct):
    x: np.ndarray
    y: np.ndarray

    def __post_init__(self):
        assert len(self.x.shape) == 2, "`x` must be a 2D array"

        # ⚠ changed to address a bug where dim-0 y numpy arrays are serialized as
        # ⚠ attributes in HDF5, which causes dimension info to be effectively lost
        if len(self.y.shape) == 0:
            object.__setattr__(self, "y", self.y.reshape(1, 1))
        elif len(self.y.shape) == 1:
            if self.x.shape[0] == 1:
                # Single row, many outputs
                object.__setattr__(self, "y", self.y.reshape(1, -1))
            elif self.y.shape[0] == self.x.shape[0]:
                # Many rows, single output
                object.__setattr__(self, "y", self.y.reshape(-1, 1))
            else:
                raise RuntimeError(
                    "Dimension mismatch between x ({}) "
                    "and y ({}) when creating Dataset.".format(
                        self.x.shape, self.y.shape
                    )
                )

        assert len(self.y.shape) == 2, "`y` must be a 2D array"
        assert (
            self.x.shape[0] == self.y.shape[0]
        ), "`x` and `y` must have the same number of rows"

    @property
    def num_samples(self):
        return self.x.shape[0]

    @property
    def num_inputs(self):
        return self.x.shape[1]

    @property
    def num_outputs(self):
        return self.y.shape[1]
