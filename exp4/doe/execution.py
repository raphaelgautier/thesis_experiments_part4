import random
import shutil
import subprocess
import time
from typing import List, Union

from exp4.cases import CaseState
from exp4.settings import STATUS_DIR
from exp4.utils import log


def get_case_to_run(worker_uid: str, testing: bool = False) -> Union[str, None]:
    # This is a hack to reduce the likelihood of race conditions between workers
    # We wait a random amount of time between 0s and 10s before picking a new case
    if not testing:
        time.sleep(random.uniform(0, 10))

    # If there are interrupted cases, we send these in priority
    case = _get_random_case_to_run_from_list(worker_uid, CaseState.INTERRUPTED)
    if case:
        log("({}) Case {} resumed.".format(worker_uid, case))
        return case

    # Otherwise we send an unstarted case
    case = _get_random_case_to_run_from_list(worker_uid, CaseState.UNSTARTED)
    if case:
        log("({}) Case {} started.".format(worker_uid, case))
        return case

    # If there were no unstarted cases either, then None is returned
    log("({}) No case to run.".format(worker_uid))
    return None


def report_interrupted(case_id: str, worker_uid: str) -> None:
    _change_state(case_id, CaseState.RUNNING, CaseState.INTERRUPTED)
    log("({}) Case {} interrupted.".format(worker_uid, case_id))


def report_finished(case_id: str, worker_uid: str) -> None:
    _change_state(case_id, CaseState.RUNNING, CaseState.FINISHED)
    log("({}) Case {} finished.".format(worker_uid, case_id))


def report_failed(case_id: str, worker_uid: str) -> None:
    _change_state(case_id, CaseState.RUNNING, CaseState.FAILED)
    log("({}) Case {} failed.".format(worker_uid, case_id))


def report_waiting(worker_uid: str) -> None:
    log("({}) Nothing to do, waiting.".format(worker_uid))


def clean_up_running_dir(worker_uid: str) -> None:
    # Logging
    log("({}) Started cleaning up the `running` directory.".format(worker_uid))

    # List all running workers
    try:
        # `grep` returns a status code of 1 if no lines are returned
        running_workers = [
            line.split(".")[0]
            for line in subprocess.check_output(
                "qstat -u rgautier6 -t | grep ' R '", shell=True, text=True
            ).split("\n")
        ]
    except subprocess.CalledProcessError:
        # if no workers are running
        running_workers = []

    # Iterate through each file in the `running` directory and change the case's state
    # to `interrupted` if its file does not contain the ID of a currently running worker
    for file_path in (STATUS_DIR / CaseState.RUNNING.value).glob("*"):
        if file_path.is_file():  # in case the file has been moved
            with open(file_path, "r") as file:
                last_subworker_uid = file.readlines()[-1]
            last_worker_uid = last_subworker_uid[: last_subworker_uid.rfind("[")]
            if last_worker_uid not in running_workers:
                try:
                    _change_state(
                        file_path.name, CaseState.RUNNING, CaseState.INTERRUPTED
                    )
                except Exception:
                    # it's okay if it fails, another worker probably cleaned up
                    # that file before
                    pass

    # Logging
    log("({}) Finished cleaning up the `running` directory.".format(worker_uid))


def _get_random_case_to_run_from_list(
    worker_uid: str, case_state: CaseState
) -> Union[str, None]:
    cases = _get_cases_by_state(case_state)
    if cases:
        case_number = random.choice(cases)
        _change_state(case_number, case_state, CaseState.RUNNING)
        with open(STATUS_DIR / CaseState.RUNNING.value / case_number, "a") as file:
            file.write("{}\n".format(worker_uid))
        return case_number
    return None


def _get_cases_by_state(case_state: CaseState) -> List[str]:
    path = STATUS_DIR / case_state.value
    return [file.name for file in path.glob("*")]


def _change_state(case_id: str, current_state: CaseState, new_state: CaseState) -> None:
    old_path = STATUS_DIR / current_state.value / case_id
    new_path = STATUS_DIR / new_state.value / case_id
    if old_path.is_file():
        shutil.move(old_path, new_path)
    else:
        raise RuntimeError(
            "Trying to change state of case {} from {} to {} failed.".format(
                case_id, current_state, new_state
            )
        )
