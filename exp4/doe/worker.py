import time
import traceback
from typing import Optional, List

from exp4.cases import CaseType
from exp4.settings import WORKER_STANDBY_INTERVAL_SECONDS

from .execution import (
    clean_up_running_dir,
    get_case_to_run,
    report_failed,
    report_finished,
    report_waiting,
)
from .run_case import run_doe_case


def worker(
    worker_uid: str,
    clean_up: bool = True,
    case_type_filter: Optional[List[CaseType]] = None,
):
    # Clean up the status directory
    if clean_up:
        try:
            clean_up_running_dir(worker_uid)
        except Exception:
            # it's not the end of the world if the clean up doesn't work
            pass

    # We keep going as long as a case number is provided
    while True:

        # Retrieve next case to run
        case_number = get_case_to_run(worker_uid)

        if case_number is None:
            # Worker waits if no case is provided
            break
            # report_waiting(worker_uid)
            # time.sleep(WORKER_STANDBY_INTERVAL_SECONDS)
        else:
            # Run the case, report if a problem occured
            try:
                run_doe_case(case_number, worker_uid, case_type_filter=case_type_filter)
                report_finished(case_number, worker_uid)
            except Exception as e:
                #     raise e
                traceback.print_stack()
                print(e)
                report_failed(case_number, worker_uid)
