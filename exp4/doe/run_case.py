from dataclasses import dataclass
from pathlib import Path
import shutil
import time
from typing import Callable, List, Optional, Tuple, cast

import dill
import h5py as h5

from exp4.cases import (
    get_case_parameters,
    CaseType,
    lf_only,
    hf_doe_is,
    hf_doe_fs,
    hf_adaptive,
    deep_mf_gp,
    ProcessParameters,
    CaseParameters,
)
from exp4.settings import TEMP_DIR, RESULTS_DIR, DOE_NAME, DEBUG
from exp4.utils import compact_timestamp, struct_to_h5group, Struct

from .process import get_process_parameters

MODULES = {
    CaseType.LF_ONLY: lf_only,
    CaseType.HF_DOE_IS: hf_doe_is,
    CaseType.HF_DOE_FS: hf_doe_fs,
    CaseType.HF_ADAPTIVE: hf_adaptive,
    CaseType.DEEP_MF_GP: deep_mf_gp,
}


def run_doe_case(
    case_id: str,
    worker_id: str,
    case_type_filter: Optional[List[CaseType]] = None,
) -> None:
    # Retrieve process and case parameters
    case_parameters = get_case_parameters(case_id)
    process_parameters = get_process_parameters(case_parameters.case_type)

    return run_case(
        case_id,
        worker_id,
        process_parameters,
        case_parameters,
        case_type_filter=case_type_filter,
    )


def run_case(
    case_id: str,
    worker_id: str,
    process_parameters: ProcessParameters,
    case_parameters: CaseParameters,
    case_type_filter: Optional[List[CaseType]] = None,
) -> None:
    # If case turns out to be over-budget, we kill it
    if case_parameters.case_cost > case_parameters.budget:
        raise RuntimeError("Case {} is over-budget.".format(case_id))

    #  Determine the case type and retrieve the corresponding module
    case_type = case_parameters.case_type
    try:
        module = MODULES[case_type]
    except KeyError:
        raise RuntimeError("Unknown case type: {}".format(case_type))

    # Temporary directory for this case
    case_temp_dir = TEMP_DIR / case_id
    case_temp_dir.mkdir(parents=True, exist_ok=True)

    # In some cases we may only want to run certain types of cases
    if case_type_filter is not None and case_type not in case_type_filter:
        raise Exception

    # Deep MF GP has to be handled separately because model cannot be pickled
    if case_type == CaseType.DEEP_MF_GP:
        process_parameters = cast(deep_mf_gp.DeepMfGpParameters, process_parameters)
        run_deep_mf_gp(
            case_id, worker_id, case_temp_dir, process_parameters, case_parameters
        )
    else:
        ###############
        # Preparation #
        ###############

        preparation_artifacts = retrieve_results_or_do(
            case_temp_dir,
            "preparation_artifacts.pkl",
            module.prepare,
            (case_temp_dir, process_parameters.preparation, case_parameters),
        )

        ############
        # Training #
        ############

        training_artifacts = retrieve_results_or_do(
            case_temp_dir,
            "training_artifacts.pkl",
            module.train,
            (
                case_temp_dir,
                process_parameters.training,
                case_parameters,
                preparation_artifacts,
            ),
        )

        ##############
        # Validation #
        ##############

        validation_artifacts = retrieve_results_or_do(
            case_temp_dir,
            "validation_artifacts.pkl",
            module.validate,
            (
                process_parameters.validation,
                case_parameters,
                preparation_artifacts,
                training_artifacts,
            ),
        )

        ################
        # Save Results #
        ################

        save(
            case_id,
            worker_id,
            case_parameters,
            process_parameters,
            preparation_artifacts,
            training_artifacts,
            validation_artifacts,
        )

        ################
        # Finalization #
        ################

        module.finalize(
            case_id, process_parameters, case_parameters, preparation_artifacts
        )

        ############
        # Clean Up #
        ############

        # Delete the temporary case directory
        if not DEBUG:
            try:
                shutil.rmtree(case_temp_dir)
            except Exception:
                # Let's not crash if clean up happens to fail for some reason
                pass


def retrieve_results_or_do(
    run_temp_dir: Path, artifacts_filename: str, func: Callable, args: Tuple
):
    # Temporary file for the training artifacts
    artifacts_filepath = run_temp_dir / artifacts_filename

    if artifacts_filepath.is_file():
        # If this step already happened, just retrieve artifacts from file
        with open(artifacts_filepath, "rb") as temp_file:
            artifacts = dill.load(temp_file)
    else:
        # Otherwise do the step and save the results
        artifacts = func(*args)

        with open(artifacts_filepath, "wb") as temp_file:
            dill.dump(artifacts, temp_file)

    return artifacts


@dataclass(frozen=True)
class CaseArtifacts(Struct):
    doe_name: str
    case_id: str
    worker_id: str
    case_parameters: CaseParameters
    process_parameters: ProcessParameters
    preparation_artifacts: Struct
    training_artifacts: Struct
    validation_artifacts: Struct


def save(
    case_id,
    worker_id,
    case_parameters,
    process_parameters,
    preparation_artifacts,
    training_artifacts,
    validation_artifacts,
) -> None:
    # Results file path
    results_file_path = RESULTS_DIR / "{}.h5".format(case_id)

    # Append timestamp if an artifact with the same name already exists
    if results_file_path.is_file():
        results_file_path = RESULTS_DIR / "{}_{}.h5".format(
            case_id, compact_timestamp()
        )

    case_artifacts = CaseArtifacts(
        doe_name=DOE_NAME,
        case_id=case_id,
        worker_id=worker_id,
        case_parameters=case_parameters,
        process_parameters=process_parameters,
        preparation_artifacts=preparation_artifacts,
        training_artifacts=training_artifacts,
        validation_artifacts=validation_artifacts,
    )

    # Write results file
    with h5.File(results_file_path, "x") as results_file:
        struct_to_h5group(case_artifacts, results_file)


def run_deep_mf_gp(
    case_id: str,
    worker_id: str,
    temp_dir: Path,
    process_parameters: deep_mf_gp.DeepMfGpParameters,
    case_parameters: CaseParameters,
):
    # Prepare
    print("Preparing...")
    start_time = time.time()
    preparation_artifacts = deep_mf_gp.prepare(
        temp_dir, process_parameters.preparation, case_parameters
    )
    print("Done preparing in {:.3f}s.".format(time.time() - start_time))

    # Train
    print("Training...")
    start_time = time.time()
    training_artifacts = deep_mf_gp.train(
        temp_dir, process_parameters.training, case_parameters, preparation_artifacts
    )
    print("Done training in {:.3f}s.".format(time.time() - start_time))

    # Validate
    print("Validating...")
    start_time = time.time()
    validation_artifacts = deep_mf_gp.validate(
        process_parameters.validation,
        case_parameters,
        preparation_artifacts,
        training_artifacts,
    )
    print("Done validating in {:.3f}s.".format(time.time() - start_time))

    # Save
    # Scrap training artifacts because we won't be able to persist the model it contains
    save(
        case_id,
        worker_id,
        case_parameters,
        process_parameters,
        preparation_artifacts,
        None,
        validation_artifacts,
    )
