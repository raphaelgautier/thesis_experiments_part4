from typing import List

from exp4.cases import CaseState, CaseParameters, add_case
from exp4.settings import (
    DOE_DIR,
    FIGURES_DIR,
    STATUS_DIR,
    RESULTS_DIR,
    TEMP_DIR,
    CASES_DIR,
)


from .process import set_process_parameters, AllProcessParameters


def initialize_doe_folder() -> None:
    # Main directory
    DOE_DIR.mkdir(parents=True, exist_ok=True)

    # Subfolders
    for subfolder in [FIGURES_DIR, STATUS_DIR, RESULTS_DIR, TEMP_DIR, CASES_DIR]:
        subfolder.mkdir(parents=True, exist_ok=True)

    # Create directories for each possible state
    for state in [state.value for state in CaseState]:
        (STATUS_DIR / state).mkdir(parents=True, exist_ok=True)


def initialize_doe(
    process_parameters: AllProcessParameters, cases: List[CaseParameters]
) -> None:
    # Initialize the DOE folder
    initialize_doe_folder()

    # Save the process parameters
    set_process_parameters(process_parameters)

    # Add all cases
    for case in cases:
        add_case(case)
