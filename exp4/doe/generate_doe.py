# from itertools import it

# import h5py as h5
# import numpy as np

# from exp4.cases import add_case
# from exp4.elliptic_pde import get_elliptic_pde_cost
# from exp4.settings import DATASETS_DIR
# from exp4.utils import Dataset

# ##################
# # DOE Generation #
# ##################

# # Preparation
# # num_inputs = case_parameters["num_inputs"]
# # lower_bounds = case_parameters["lower_bounds"]
# # upper_bounds = case_parameters["upper_bounds"]
# # lf_process_name = case_parameters["lf_process_name"]
# # lf_process_parameters = case_parameters["lf_process_parameters"]
# # lf_doe_random_seed = case_parameters["lf_doe_random_seed"]
# # num_lf_samples = case_parameters["num_lf_samples"]


# # Training
# # mcmc_parameters = process_parameters["lf_only"]["mcmc_parameters"]
# # dim_feature_space = case_parameters["dim_feature_space"]

# # Validation
# # validation_parameters = process_parameters["validation_parameters"]
# # val_pred_rand_seed = validation_parameters["predictions_random_seed"]
# # ci_bounds = validation_parameters["confidence_interval_bounds"]
# # num_gp_samples = validation_parameters["num_gp_samples"]

# # Retrieve data needed for validation
# # lf_process_name = case_parameters["lf_process_name"]
# # hf_process_name = case_parameters["hf_process_name"]
# # lf_training_data = preparation_artifacts["lf_training_data"]

# mcmc_parameters = ...

# case_parameters = {
#     "lf_process_parameters": ...,
#     "hf_process_parameters": ...,
#     "dim_feature_space": ...,
#     "num_inputs": ...,
#     "lower_bounds": ...,
#     "upper_bounds": ...,
#     "lf_doe_random_seed": ...,
#     "num_lf_samples": ...,
#     "hf_doe_random_seed": ...,
#     "adaptive_sampling_random_seed": ...,
# }

# process_parameters = {
#     "training_parameters": {
#         "ridge_bgp": {"mcmc_parameters": mcmc_parameters},
#         "deep_mf_gp": ...,
#     },
#     "validation_parameters": {
#         "predictions_random_seed": ...,
#         "confidence_interval_bounds": ...,
#         "num_gp_samples": ...,
#     },
# }

# if __name__ == "__main__":
#     dataset_pairs = []
#     alt_budgets = ...
#     repetitions = []


# def get_number(budget_in_hf_samples, lf_allocation_ratio):

#     budget = budget_in_hf_samples * hf_cost
#     num_hf_training_samples = int((1 - lf_allocation_ratio) * budget_in_hf_samples)
#     num_lf_training_samples = int(
#         lf_allocation_ratio * hf_cost / lf_cost * budget_in_hf_samples
#     )


# def initialize_doe(dataset_pairs, repetitions):
#     # Initialize case ID
#     case_id = 0

#     # Iterate through each dataset pair
#     for pair_name, pair_info in dataset_pairs.items():

#         # Retrieve LF and HF dataset names
#         lf_process = pair_info["lf"]
#         hf_process = pair_info["hf"]

#         # Retrieve needed dataset metadata
#         # 💡 We assume LF and HF datasets have the same input domain
#         with h5.File(DATASETS_DIR / "{}.h5".format(lf_process), "r") as lf_dataset:
#             lf_cost = np.mean(lf_dataset["durations"])

#         with h5.File(DATASETS_DIR / "{}.h5".format(hf_process), "r") as hf_dataset:
#             hf_cost = np.mean(hf_dataset["durations"])

#         # Generate all cases for this pair of datasets
#         for budget_in_hf_samples, repetition in it.product(
#             pair_info["total_cost_sweep_in_hf_samples"], repetitions
#         ):
#             case_parameters = {
#                 "dataset_pair": pair_name,
#                 "lf_process": lf_process,
#                 "hf_process": hf_process,
#                 "budget_in_hf_samples": budget_in_hf_samples,
#                 "cost_ratio": hf_cost / lf_cost,
#                 "allocation_ratio": allocation_ratio,
#                 "num_lf_training_samples": num_lf_training_samples,
#                 "num_hf_training_samples": num_hf_training_samples,
#                 "lf_dataset_split_random_seed": dataset_split_random_seed[0],
#                 "hf_dataset_split_random_seed": dataset_split_random_seed[1],
#                 "feature_space_relation": feature_space_relation,
#                 "dim_feature_space": dim_fs,
#             }
#             add_case(str(case_id))
#             case_id += 1
