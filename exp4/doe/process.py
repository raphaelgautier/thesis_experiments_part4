from dataclasses import dataclass
import json

from exp4.cases import (
    CaseType,
    LfOnlyParameters,
    HfDoeFsParameters,
    HfDoeIsParameters,
    HfAdaptiveParameters,
    DeepMfGpParameters,
    ProcessParameters,
)
from exp4.settings import PROCESS_PARAMETERS_FILE
from exp4.utils import pydict_to_struct, struct_to_pydict, Struct


@dataclass()
class AllProcessParameters(Struct):
    lf_only: LfOnlyParameters
    hf_doe_is: HfDoeIsParameters
    hf_doe_fs: HfDoeFsParameters
    hf_adaptive: HfAdaptiveParameters
    deep_mf_gp: DeepMfGpParameters


def set_process_parameters(process_parameters: AllProcessParameters):
    with open(PROCESS_PARAMETERS_FILE, "w") as process_params_file:
        json.dump(struct_to_pydict(process_parameters), process_params_file, indent=2)


def get_process_parameters(case_type: CaseType) -> ProcessParameters:
    with open(PROCESS_PARAMETERS_FILE) as file:
        dict_process_parameters = json.load(file)
    all_process_parameters = pydict_to_struct(
        dict_process_parameters, AllProcessParameters
    )

    if case_type == CaseType.LF_ONLY:
        return all_process_parameters.lf_only
    elif case_type == CaseType.HF_DOE_IS:
        return all_process_parameters.hf_doe_is
    elif case_type == CaseType.HF_DOE_FS:
        return all_process_parameters.hf_doe_fs
    elif case_type == CaseType.HF_ADAPTIVE:
        return all_process_parameters.hf_adaptive
    elif case_type == CaseType.DEEP_MF_GP:
        return all_process_parameters.deep_mf_gp
