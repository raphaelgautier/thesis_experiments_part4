from pathlib import Path

from decouple import AutoConfig

###########################
# Configuration Constants #
###########################

config = AutoConfig(search_path=Path())

# Note: the `decouple` library does not play well with typing

DOE_NAME: str = config("EXP4_DOE_NAME")  # type: ignore
DATA_DIR: Path = config("EXP4_DATA_DIR", cast=Path)  # type: ignore
DATASETS_DIR: Path = DATA_DIR / "datasets"

# DOE-specific
DOE_DIR: Path = DATA_DIR / "does" / DOE_NAME
STATUS_DIR: Path = DOE_DIR / "status"
TEMP_DIR: Path = DOE_DIR / "temp"
RESULTS_DIR: Path = DOE_DIR / "results"
FIGURES_DIR: Path = DOE_DIR / "figures"
LOG_FILE: Path = DOE_DIR / "workers.log"
PROCESS_PARAMETERS_FILE: Path = DOE_DIR / "process_parameters.json"
CASES_DIR: Path = DOE_DIR / "cases"

# Other settings
WORKER_STANDBY_INTERVAL_SECONDS: float = config(
    "WORKER_STANDBY_INTERVAL_SECONDS", default=60 * 10, cast=float
)  # type: ignore
DEBUG: bool = config("DEBUG", cast=bool)  # type: ignore
