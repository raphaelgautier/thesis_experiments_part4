from dataclasses import dataclass
from pathlib import Path

import matplotlib
import numpy as np

from exp4.elliptic_pde import get_validation_data
from exp4.models import (
    mf_deep_ridge_gp_train,
    mf_deep_ridge_gp_predict,
    DeepMfRidgeGpTrainingArtifacts,
    McmcParameters,
)
from exp4.models.multifidelity_deep_gp_ridge import FsRelation
from exp4.utils import Dataset, Struct

from .validation import (
    ValidationParameters,
    compute_validation_metrics,
    ValidationMetrics,
)
from .doe import CaseParameters

matplotlib.use("pdf")


##############
# Parameters #
##############


@dataclass(frozen=True)
class MfTrainingParameters(Struct):
    mcmc: McmcParameters


@dataclass(frozen=True)
class MfValidationParameters(ValidationParameters):
    num_lf_predictions_mf_model: int = 10
    lf_predictions_seed_mf_model: int = 0


#############
# Artifacts #
#############


@dataclass(frozen=True)
class MfPreparationArtifacts(Struct):
    lf_training_data: Dataset
    hf_training_data: Dataset


MfTrainingArtifacts = DeepMfRidgeGpTrainingArtifacts


@dataclass(frozen=True)
class MfValidationArtifacts(Struct):
    lf_training: ValidationMetrics
    lf_validation: ValidationMetrics
    hf_training: ValidationMetrics
    hf_validation: ValidationMetrics
    hf_actual_vs_lf_predicted: ValidationMetrics


############
# Routines #
############


def train_mf(
    case_temp_dir: Path,
    training_parameters: MfTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: MfPreparationArtifacts,
) -> MfTrainingArtifacts:
    # Retrieve training parameters
    lf_training_data = preparation_artifacts.lf_training_data
    hf_training_data = preparation_artifacts.hf_training_data
    mcmc_parameters = training_parameters.mcmc
    dim_feature_space = case_parameters.dim_feature_space
    mcmc_file_path = case_temp_dir / "mcmc.pkl"

    return mf_deep_ridge_gp_train(
        lf_training_data,
        hf_training_data,
        mcmc_parameters,
        FsRelation.SHARED,
        dim_feature_space,
        mcmc_file_path=mcmc_file_path,
    )


def validate_mf(
    validation_parameters: MfValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: MfPreparationArtifacts,
    training_artifacts: MfTrainingArtifacts,
) -> MfValidationArtifacts:
    # Problem dimensions
    num_hf_training_samples = preparation_artifacts.hf_training_data.num_samples
    dim_feature_space = case_parameters.dim_feature_space

    # Retrieve validation parameters
    num_lf_predictions_mf_model = validation_parameters.num_lf_predictions_mf_model
    lf_predictions_seed_mf_model = validation_parameters.lf_predictions_seed_mf_model

    # Retrieve data needed for validation
    lf_training_data = preparation_artifacts.lf_training_data
    hf_training_data = preparation_artifacts.hf_training_data
    lf_validation_data = get_validation_data(case_parameters.lf_process)
    hf_validation_data = get_validation_data(case_parameters.hf_process)

    def predict(pred_x):
        # pred_x should be a 2D array
        assert len(pred_x.shape) == 2

        # Preparing random draws for intermediate LF draws
        # Standard gaussian samples used to propagate the LF output uncertainty to HF
        num_predictions = pred_x.shape[0]
        rng = np.random.default_rng(lf_predictions_seed_mf_model)
        std_gaussian_samples = rng.standard_normal(
            size=(
                num_lf_predictions_mf_model,
                num_hf_training_samples + num_predictions,
            )
        )

        return mf_deep_ridge_gp_predict(
            pred_x,
            lf_training_data,
            hf_training_data,
            training_artifacts,
            dim_feature_space,
            std_gaussian_samples,
        )

    def validation_routine(means, variances, actual):
        actual = actual.reshape((-1, 1))  # need a column vector
        return compute_validation_metrics(
            means, variances, actual, validation_parameters
        )

    # Make predictions
    lf_train_means, lf_train_variances, _, _ = predict(lf_training_data.x)
    lf_val_means, lf_val_variances, _, _ = predict(lf_validation_data.x)
    _, _, hf_train_means, hf_train_variances = predict(hf_training_data.x)
    (
        lf_means_at_hf_val,
        lf_variances_at_hf_val,
        hf_val_means,
        hf_val_variances,
    ) = predict(hf_validation_data.x)

    return MfValidationArtifacts(
        lf_training=validation_routine(
            lf_train_means, lf_train_variances, lf_training_data.y
        ),
        lf_validation=validation_routine(
            lf_val_means, lf_val_variances, lf_validation_data.y
        ),
        hf_training=validation_routine(
            hf_train_means, hf_train_variances, hf_training_data.y
        ),
        hf_validation=validation_routine(
            hf_val_means, hf_val_variances, hf_validation_data.y
        ),
        hf_actual_vs_lf_predicted=validation_routine(
            lf_means_at_hf_val, lf_variances_at_hf_val, hf_validation_data.y
        ),
    )
