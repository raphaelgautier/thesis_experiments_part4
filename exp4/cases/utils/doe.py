from dataclasses import dataclass
import enum
import json
from multiprocessing import Pool
from typing import Dict, Union

from tqdm import tqdm

from exp4.settings import STATUS_DIR, CASES_DIR
from exp4.elliptic_pde import EllipticPDEParameters
from exp4.utils import Struct, pydict_to_struct, struct_to_pydict


class CaseType(str, enum.Enum):
    LF_ONLY = "lf_only"
    HF_DOE_IS = "hf_doe_is"
    HF_DOE_FS = "hf_doe_fs"
    HF_ADAPTIVE = "hf_adaptive"
    DEEP_MF_GP = "deep_mf_gp"


@dataclass(frozen=True)
class CaseParameters(Struct):
    # Case identification
    case_type: CaseType
    case_id: str
    parent_case_id: Union[str, None]
    # Feature space
    dim_feature_space: int
    # Budget and budget allocation
    budget: float
    case_cost: float
    lf_doe_budget_fraction: float
    hf_doe_budget_fraction: float
    hf_adaptive_budget_fraction: float
    # Test Process
    lf_process: EllipticPDEParameters
    hf_process: EllipticPDEParameters
    # Random seeds
    lf_doe_random_seed: int
    hf_doe_random_seed: int
    adaptive_sampling_random_seed: int


class CaseState(str, enum.Enum):
    UNSTARTED = "unstarted"
    RUNNING = "running"
    FINISHED = "finished"
    INTERRUPTED = "interrupted"
    FAILED = "failed"


def add_case(case_parameters: CaseParameters):
    # Add file with case parameters
    case_id = case_parameters.case_id
    case_filename = CASES_DIR / "{}.json".format(case_id)
    with open(case_filename, "w") as case_file:
        json.dump(struct_to_pydict(case_parameters), case_file, indent=2)

    # Add file to the file-based progress tracking system
    status_filename = STATUS_DIR / CaseState.UNSTARTED.value / case_id
    status_filename.touch()


def get_all_cases() -> Dict[str, CaseParameters]:
    all_case_ids = [case_file_path.stem for case_file_path in CASES_DIR.glob("*.json")]
    case_parameters = list(
        tqdm(
            Pool().imap(get_case_parameters, all_case_ids),
            desc="Retrieving all cases",
            total=len(all_case_ids),
        )
    )
    return dict(zip(all_case_ids, case_parameters))

    # cases = {}
    # for case_file_path in CASES_DIR.glob("*.json"):
    #     case_id = case_file_path.stem
    #     cases[case_id] = get_case_parameters(case_id)
    # return cases


def get_case_parameters(case_id: str) -> CaseParameters:
    with open(CASES_DIR / "{}.json".format(case_id), "r") as file:
        case_parameters = pydict_to_struct(json.load(file), CaseParameters)
    return case_parameters
