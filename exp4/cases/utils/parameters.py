from dataclasses import dataclass
from typing import Any

from exp4.utils import Struct


@dataclass(frozen=True)
class ProcessParameters(Struct):
    preparation: Any
    training: Any
    validation: Any
