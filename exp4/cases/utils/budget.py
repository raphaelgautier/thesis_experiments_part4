from dataclasses import replace

from exp4.elliptic_pde import get_elliptic_pde_cost

from .doe import CaseParameters, CaseType, add_case


def compute_num_lf_doe_samples(case_parameters: CaseParameters):
    lf_process_cost = get_elliptic_pde_cost(case_parameters.lf_process)
    lf_doe_budget = case_parameters.lf_doe_budget_fraction * case_parameters.budget
    num_lf_doe_cases = int(lf_doe_budget // lf_process_cost)
    num_lf_doe_cases = num_lf_doe_cases if num_lf_doe_cases > 0 else 1
    return num_lf_doe_cases


def compute_num_hf_doe_samples(case_parameters: CaseParameters):
    hf_process_cost = get_elliptic_pde_cost(case_parameters.hf_process)
    stepwise_budget_fraction = 0.1  # ⚠ hard-coded, should be a parameter
    budget_per_step = stepwise_budget_fraction * case_parameters.budget
    num_hf_samples_per_step = int(budget_per_step // hf_process_cost)
    num_hf_samples_per_step = (
        num_hf_samples_per_step if num_hf_samples_per_step > 0 else 1
    )
    num_steps = int(case_parameters.hf_doe_budget_fraction // stepwise_budget_fraction)
    num_hf_doe_cases = num_steps * num_hf_samples_per_step

    return num_hf_doe_cases


def compute_num_hf_adaptive_samples(
    case_parameters: CaseParameters, budget_fraction_per_step=0.1
):
    hf_process_cost = get_elliptic_pde_cost(case_parameters.hf_process)
    hf_adaptive_budget = budget_fraction_per_step * case_parameters.budget
    num_hf_samples = int(hf_adaptive_budget // hf_process_cost)
    num_hf_samples = num_hf_samples if num_hf_samples > 0 else 1
    return num_hf_samples


def compute_cumulative_num_hf_adaptive_evaluations(
    case_parameters: CaseParameters, budget_fraction_per_step: float = 0.1
):
    # Compute budget per step and number of steps
    num_as_steps = int(
        case_parameters.hf_adaptive_budget_fraction / budget_fraction_per_step
    )
    num_hf_evals_per_step = compute_num_hf_adaptive_samples(
        case_parameters, budget_fraction_per_step=budget_fraction_per_step
    )
    num_hf_as_evals = num_hf_evals_per_step * num_as_steps

    return num_hf_as_evals


def compute_total_case_cost(
    case_parameters: CaseParameters, hf_adaptive_budget_step: float = 0.1
):
    # Initialization
    num_lf_evaluations = 0
    num_hf_evaluations = 0

    # LF DOE Budget - all cases
    num_lf_evaluations += compute_num_lf_doe_samples(case_parameters)

    # HF DOE budget - all except LF only
    if case_parameters.case_type != CaseType.LF_ONLY:
        num_hf_evaluations += compute_num_hf_doe_samples(case_parameters)

    # Adaptive sampling budget - only for adaptive case
    if case_parameters.case_type == CaseType.HF_ADAPTIVE:
        num_hf_evaluations += compute_cumulative_num_hf_adaptive_evaluations(
            case_parameters, hf_adaptive_budget_step
        )

    # Costs
    lf_cost = num_lf_evaluations * get_elliptic_pde_cost(case_parameters.lf_process)
    hf_cost = num_hf_evaluations * get_elliptic_pde_cost(case_parameters.hf_process)
    total_case_cost = lf_cost + hf_cost

    return total_case_cost


def check_case_cost_and_add_case(
    new_case_parameters: CaseParameters, hf_adaptive_budget_step: float = 0.1
):
    # We compute the **actual** cost of the case to be spawn
    case_cost = compute_total_case_cost(
        new_case_parameters, hf_adaptive_budget_step=hf_adaptive_budget_step
    )

    # Add the case if we have enough budget
    if case_cost <= new_case_parameters.budget:
        new_case_parameters = replace(new_case_parameters, case_cost=case_cost)
        add_case(new_case_parameters)
