from dataclasses import dataclass
from typing import Tuple

from jax import jit, numpy as jnp
from jax.random import PRNGKey
from jax.scipy.special import logsumexp
import numpy as np
from numpyro import distributions as dist

from exp4.utils import Struct

######################
# Validation Metrics #
######################


@dataclass(frozen=True)
class ValidationParameters(Struct):
    random_seed: int = 0
    num_draws_per_gaussian: int = 10
    ci_bounds: Tuple[float, float] = (0.025, 0.975)


@dataclass(frozen=True)
class ValidationMetrics(Struct):
    rmse: float
    nrmse: float
    r_squared: float
    mlppd: float
    point_based_predictions: np.ndarray
    confidence_interval_bounds: np.ndarray
    absolute_pointwise_error: np.ndarray
    lppd: np.ndarray


def compute_validation_metrics(
    predicted_means: np.ndarray,
    predicted_variances: np.ndarray,
    actual_y: np.ndarray,
    validation_parameters: ValidationParameters,
) -> ValidationMetrics:
    # Quantile values need to be arrays
    confidence_interval_bounds_cdf_values = jnp.array(validation_parameters.ci_bounds)

    # We start with point-based predictions and confidence interval bounds
    # Depending on whether the method is fully Bayesian or not, the computations can be
    # made analytically or require sampling the posterior predictive distribution
    if predicted_means.shape[1] == 1:
        point_based_predictions = predicted_means
        confidence_interval_bounds = dist.Normal(
            predicted_means, jnp.sqrt(predicted_variances)
        ).icdf(confidence_interval_bounds_cdf_values)
    else:
        prng_key = PRNGKey(validation_parameters.random_seed)
        num_samples = validation_parameters.num_draws_per_gaussian
        num_predictions = predicted_means.shape[0]
        num_posterior_draws = predicted_means.shape[1]
        samples = (
            dist.Normal(loc=predicted_means, scale=jnp.sqrt(predicted_variances))
            .sample(prng_key, sample_shape=(num_samples,))
            .transpose((1, 2, 0))
            .reshape((num_predictions, num_posterior_draws * num_samples))
        )

        quantiles = jnp.quantile(
            samples,
            jnp.concatenate((jnp.array([0.5]), confidence_interval_bounds_cdf_values)),
            axis=1,
        ).T

        point_based_predictions = quantiles[:, 0, None]
        confidence_interval_bounds = quantiles[:, 1:]

    # Global error metrics
    (
        rmse,
        nrmse,
        r_squared,
        absolute_pointwise_error,
    ) = rmse_nrmse_r_squared_and_absolute_error(point_based_predictions, actual_y)

    mlppd, lppd = mean_log_pointwise_predictive_density_and_lppd(
        predicted_means, predicted_variances, actual_y
    )

    return ValidationMetrics(
        rmse=rmse,
        nrmse=nrmse,
        r_squared=r_squared,
        mlppd=mlppd,
        point_based_predictions=point_based_predictions,
        confidence_interval_bounds=confidence_interval_bounds,
        absolute_pointwise_error=absolute_pointwise_error,
        lppd=lppd,
    )


#################
# Error Metrics #
#################


@jit
def rmse_nrmse_r_squared_and_absolute_error(y_predicted, y_actual):
    var_y_actual = jnp.var(y_actual)
    absolute_error = (y_predicted - y_actual).flatten()
    mse = jnp.mean(jnp.square(absolute_error))
    r_squared = 1 - mse / var_y_actual
    rmse = jnp.sqrt(mse)
    nrmse = rmse / jnp.sqrt(var_y_actual)
    return rmse, nrmse, r_squared, absolute_error


@jit
def mean_log_pointwise_predictive_density_and_lppd(y_means, y_variances, y_actual):
    lppd = (
        logsumexp(
            dist.Normal(loc=y_means, scale=jnp.sqrt(y_variances)).log_prob(y_actual),
            axis=1,
        )
        - jnp.log(y_means.shape[1])
    )
    mlppd = jnp.mean(lppd, axis=0)
    return mlppd, lppd
