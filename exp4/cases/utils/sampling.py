from pathlib import Path
from pprint import pprint
from typing import cast

import dill
from jax import jit, grad
import matplotlib
import numpy as np
from scipy.optimize import minimize
from scipy import stats
from tqdm import tqdm
from exp4.cases.utils.budget import (
    compute_num_hf_doe_samples,
    compute_num_lf_doe_samples,
)

from exp4.elliptic_pde import evaluate_elliptic_pde, EllipticPDEParameters
from exp4.models import householder_reparameterization
from exp4.settings import DEBUG
from exp4.utils import compact_timestamp, Dataset


from .multi_fidelity import MfPreparationArtifacts
from .doe import CaseParameters

matplotlib.use("pdf")


from matplotlib import pyplot as plt  # noqa: E402


def lhs_doe(
    temp_dir: Path,
    num_samples: int,
    process: EllipticPDEParameters,
    doe_random_seed: int,
) -> Dataset:
    # Extract relevant parameters
    # process_cost = get_elliptic_pde_cost(process)
    num_inputs = process.num_inputs
    lower_bounds = process.lower_bounds
    upper_bounds = process.upper_bounds

    # # Number of samples dictated by case budget
    # num_samples = int(case_budget // process_cost)

    # Select LF training data using DOE in IS
    lhs_sampler = stats.qmc.LatinHypercube(d=num_inputs, seed=doe_random_seed)
    x = lhs_sampler.random(n=num_samples)
    x = stats.qmc.scale(x, lower_bounds, upper_bounds)

    # Evaluate the model at the locations
    y = evaluate_elliptic_pde(temp_dir, x, process)
    y = cast(np.ndarray, y)

    return Dataset(x=x, y=y)


def lf_doe_is(temp_dir: Path, case_parameters: CaseParameters) -> Dataset:
    num_samples = compute_num_lf_doe_samples(case_parameters)
    return lhs_doe(
        temp_dir,
        num_samples,
        case_parameters.lf_process,
        case_parameters.lf_doe_random_seed,
    )


def hf_doe_is(temp_dir: Path, case_parameters: CaseParameters) -> Dataset:
    num_samples = compute_num_hf_doe_samples(case_parameters)
    return lhs_doe(
        temp_dir,
        num_samples,
        case_parameters.hf_process,
        case_parameters.hf_doe_random_seed,
    )


def lf_and_hf_doe_is(
    temp_dir: Path, case_parameters: CaseParameters
) -> MfPreparationArtifacts:
    return MfPreparationArtifacts(
        lf_training_data=lf_doe_is(temp_dir, case_parameters),
        hf_training_data=hf_doe_is(temp_dir, case_parameters),
    )


def optimize_input_sites(
    obj_func,
    num_hf_samples,
    lower_bounds,
    upper_bounds,
    num_optim_restarts,
    optim_restarts_seed,
    saved_state_filepath=None,
) -> np.ndarray:
    # Load current state if applicable
    if saved_state_filepath is not None and saved_state_filepath.is_file():
        # If it does, we use it to retrieve the latest MCMC state
        with open(saved_state_filepath, "rb") as file:
            saved_state = dill.load(file)
        last_restart_number = saved_state["last_restart_number"]
        best_obj = saved_state["best_obj"]
        best_x = saved_state["best_x"]
    else:
        last_restart_number = -1
        best_obj = np.inf
        best_x = None

    # Dimensions
    dim_inputs = len(lower_bounds)

    # Initialization
    loc = np.array(lower_bounds).reshape((1, -1))
    scale = np.subtract(np.array(upper_bounds), np.array(lower_bounds)).reshape((1, -1))
    rng = np.random.default_rng(optim_restarts_seed)

    # Wrap the objective function
    obj = jit(obj_func)

    def objective(x):
        return obj(x.reshape(num_hf_samples, dim_inputs)).astype(float)

    jac = jit(grad(objective))

    def jacobian(x):
        return np.array(jac(x))

    # Format input bounds as expected by scipy.minimize
    bounds = [
        (lower_bounds[i % dim_inputs], upper_bounds[i % dim_inputs])
        for i in range(dim_inputs * num_hf_samples)
    ]

    # Options
    method = "L-BFGS-B"
    options = {"maxiter": 1000, "ftol": 1e-2}

    # method = "SLSQP"
    # options = {"maxiter": 1000, "ftol": 1e-2}

    # Optimization restarts
    for restart_number in tqdm(range(num_optim_restarts), disable=(not DEBUG)):
        # Select a new starting point
        # We don't skip this even when resuming from saved state in order to restore
        # the random number generator's state
        x0 = stats.uniform().rvs(
            size=(num_hf_samples, dim_inputs), random_state=rng
        )  # type: ignore
        x0 = (loc + x0 * scale).reshape((-1,))

        if restart_number > last_restart_number:
            # Do the actual optim
            res = minimize(
                objective,
                x0,
                method=method,
                jac=jacobian,
                bounds=bounds,
                options=options,
            )

            if DEBUG:
                pprint(res)
                print(res["fun"], best_obj)

            # We retain the latest results if it's better than what we had
            if res["fun"] < best_obj:
                best_obj = res["fun"]
                best_x = res["x"].reshape((num_hf_samples, dim_inputs))

            # Save the state for resuming later
            with open(saved_state_filepath, "wb") as file:
                dill.dump(
                    {
                        "last_restart_number": restart_number,
                        "best_obj": best_obj,
                        "best_x": best_x,
                    },
                    file,
                )

    return best_x  # type: ignore


def plot_design_in_fs(temp_dir, projection_parameters_posterior_samples, dim_fs, x):
    # Dimensions
    dim_inputs = x.shape[1]

    # Create figures
    for i in range(projection_parameters_posterior_samples.shape[0]):
        w = householder_reparameterization(
            projection_parameters_posterior_samples[i], dim_inputs, dim_fs
        )
        plt.figure()
        plt.scatter((x @ w)[:, 0], (x @ w)[:, 1])
        plt.savefig(temp_dir / "{}_doe_fs_{}.png".format(compact_timestamp(), i))
        plt.close()
