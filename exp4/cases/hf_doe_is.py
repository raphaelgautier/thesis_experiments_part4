from dataclasses import dataclass
from pathlib import Path

from jax import config as jax_config

from exp4.utils import Struct

from .utils.doe import CaseParameters
from .utils.multi_fidelity import (
    MfPreparationArtifacts,
    train_mf,
    validate_mf,
    MfTrainingParameters,
    MfValidationParameters,
    MfTrainingArtifacts,
    MfValidationArtifacts,
)
from .utils.parameters import ProcessParameters
from .utils.sampling import lf_and_hf_doe_is

jax_config.update("jax_enable_x64", True)

##############
# Parameters #
##############


HfDoeIsPreparationParameters = Struct

HfDoeIsTrainingParameters = MfTrainingParameters

HfDoeIsValidationParameters = MfValidationParameters


@dataclass(frozen=True)
class HfDoeIsParameters(ProcessParameters):
    preparation: HfDoeIsPreparationParameters
    training: HfDoeIsTrainingParameters
    validation: HfDoeIsValidationParameters


#############
# Artifacts #
#############


HfDoeIsPreparationArtifacts = MfPreparationArtifacts

HfDoeIsTrainingArtifacts = MfTrainingArtifacts

HfDoeIsValidationArtifacts = MfValidationArtifacts


def prepare(
    temp_dir: Path,
    preparation_parameters: HfDoeIsPreparationParameters,
    case_parameters: CaseParameters,
) -> HfDoeIsPreparationArtifacts:
    # Select LF training data and HF training data using a DOE in IS
    return lf_and_hf_doe_is(temp_dir, case_parameters)


def train(
    case_temp_dir: Path,
    training_parameters: HfDoeIsTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeIsPreparationArtifacts,
) -> HfDoeIsTrainingArtifacts:
    return train_mf(
        case_temp_dir, training_parameters, case_parameters, preparation_artifacts
    )


def validate(
    validation_parameters: HfDoeIsValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeIsPreparationArtifacts,
    training_artifacts: HfDoeIsTrainingArtifacts,
) -> HfDoeIsValidationArtifacts:
    return validate_mf(
        validation_parameters,
        case_parameters,
        preparation_artifacts,
        training_artifacts,
    )


def finalize(
    case_id: str,
    process_parameters: ProcessParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeIsPreparationArtifacts,
) -> None:
    # Nothing to do
    pass
