from dataclasses import dataclass, replace
from functools import partial
from pathlib import Path
from typing import cast

import h5py
from jax import numpy as jnp, vmap, config as jax_config
import numpy as np

from exp4.elliptic_pde import evaluate_elliptic_pde
from exp4.models.utils.gp import squared_distance
from exp4.models.utils.householder import householder_reparameterization
from exp4.settings import RESULTS_DIR
from exp4.utils import Dataset, Struct, h5group_to_struct

from .utils.budget import check_case_cost_and_add_case, compute_num_hf_doe_samples
from .utils.doe import CaseParameters, CaseType
from .utils.multi_fidelity import (
    MfPreparationArtifacts,
    MfTrainingArtifacts,
    MfTrainingParameters,
    MfValidationArtifacts,
    MfValidationParameters,
    train_mf,
    validate_mf,
)
from .utils.parameters import ProcessParameters
from .utils.sampling import optimize_input_sites, lhs_doe

jax_config.update("jax_enable_x64", True)

##############
# Parameters #
##############


@dataclass(frozen=True)
class HfDoeFsPreparationParameters(Struct):
    fs_component_optim_weight: float
    num_optim_restarts: int


HfDoeFsTrainingParameters = MfTrainingParameters

HfDoeFsValidationParameters = MfValidationParameters


@dataclass(frozen=True)
class HfDoeFsParameters(ProcessParameters):
    preparation: HfDoeFsPreparationParameters
    training: HfDoeFsTrainingParameters
    validation: HfDoeFsValidationParameters


#############
# Artifacts #
#############

HfDoeFsPreparationArtifacts = MfPreparationArtifacts

HfDoeFsTrainingArtifacts = MfTrainingArtifacts

HfDoeFsValidationArtifacts = MfValidationArtifacts

############
# Routines #
############


def prepare(
    temp_dir: Path,
    preparation_parameters: HfDoeFsPreparationParameters,
    case_parameters: CaseParameters,
) -> HfDoeFsPreparationArtifacts:
    # Retrieve LF samples and model from parent case ###################################
    parent_case_id = case_parameters.parent_case_id
    with h5py.File(RESULTS_DIR / "{}.h5".format(parent_case_id)) as file:
        # LF training data
        lf_training_data_h5 = file["preparation_artifacts/lf_training_data"]
        assert type(lf_training_data_h5) is h5py.Group
        lf_training_data = h5group_to_struct(lf_training_data_h5, Dataset)

        # Posterior draws of the projection parameters
        post_draws_h5 = file["training_artifacts/posterior_draws/projection_parameters"]
        projection_parameters_posterior_samples = np.array(post_draws_h5)

    # Select HF training data using DOE in FS ##########################################

    # Retrieve relevant process parameters
    fs_weight = preparation_parameters.fs_component_optim_weight
    num_optim_restarts = preparation_parameters.num_optim_restarts

    # Retrieve relevant case parameters
    dim_fs = case_parameters.dim_feature_space
    lower_bounds = case_parameters.hf_process.lower_bounds
    upper_bounds = case_parameters.hf_process.upper_bounds
    hf_process = case_parameters.hf_process
    hf_doe_random_seed = case_parameters.hf_doe_random_seed
    num_hf_samples = compute_num_hf_doe_samples(case_parameters)

    if num_hf_samples < 1:
        # This shouldn't happen
        raise RuntimeError("HF DOE FS: number of samples < 1")
    elif num_hf_samples == 1:
        # No need for a DOE really, just easier to implement
        hf_training_data = lhs_doe(
            temp_dir, num_hf_samples, hf_process, hf_doe_random_seed
        )
    else:
        # Select LF training data using custom DOE
        hf_training_x = doe_fs(
            temp_dir,
            num_hf_samples,
            lower_bounds,
            upper_bounds,
            fs_weight,
            dim_fs,
            projection_parameters_posterior_samples,
            hf_doe_random_seed,
            num_optim_restarts,
        )

        # Evaluate the model at the locations
        hf_training_y = evaluate_elliptic_pde(temp_dir, hf_training_x, hf_process)
        hf_training_y = cast(np.ndarray, hf_training_y)
        hf_training_data = Dataset(x=hf_training_x, y=hf_training_y)

    return HfDoeFsPreparationArtifacts(
        lf_training_data=lf_training_data, hf_training_data=hf_training_data
    )


def train(
    case_temp_dir: Path,
    training_parameters: HfDoeFsTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeFsPreparationArtifacts,
) -> HfDoeFsTrainingArtifacts:
    return train_mf(
        case_temp_dir, training_parameters, case_parameters, preparation_artifacts
    )


def validate(
    validation_parameters: HfDoeFsValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeFsPreparationArtifacts,
    training_artifacts: HfDoeFsTrainingArtifacts,
) -> HfDoeFsValidationArtifacts:
    return validate_mf(
        validation_parameters,
        case_parameters,
        preparation_artifacts,
        training_artifacts,
    )


def finalize(
    case_id: str,
    process_parameters: ProcessParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfDoeFsPreparationArtifacts,
):
    # We spawn a new DOE case that depends on this case.
    # Now that we have both LF and HF samples available, this is the first case using
    # adaptive sampling in the FS to select new HF samples.
    # This case inherits the same case parameters.

    # # Compute remaining actual and allocated budgets for the child cases
    # hf_cost = get_elliptic_pde_cost(case_parameters.hf_process)
    # spent_budget_fraction = (
    #     case_parameters.lf_doe_budget_fraction
    #     + case_parameters.hf_doe_budget_fraction
    # )
    # remaining_budget = (1 - spent_budget_fraction) * case_parameters.budget

    # # If we can at least sample one more HF
    # if remaining_budget > hf_cost:

    # Parameters for the new case
    new_case_parameters = replace(
        case_parameters,
        case_type=CaseType.HF_ADAPTIVE,
        case_id="{}-ADAPT1".format(case_id),
        parent_case_id=case_id,
        hf_adaptive_budget_fraction=0.1,
    )
    check_case_cost_and_add_case(new_case_parameters)


##########
# FS DOE #
##########


def fs_comp_single_post_draw(dim_inputs, dim_fs, lower_tri_indices, x, proj_params):
    z = x @ householder_reparameterization(proj_params, dim_inputs, dim_fs)
    return jnp.sqrt(jnp.min(squared_distance(z, z)[lower_tri_indices]) / dim_fs)


def doe_objective(fs_comp_weight, dim_fs, proj_params_post_draws, lower_tri_indices, x):
    # Dimension
    dim_inputs = x.shape[1]

    # First componenent of the DOE selection criterion
    is_comp = jnp.sqrt(jnp.min(squared_distance(x, x)[lower_tri_indices]) / dim_inputs)
    # is_comp = 0

    # Second component of the DOE selection criterion
    fs_comp = jnp.mean(
        vmap(fs_comp_single_post_draw, in_axes=(None, None, None, None, 0))(
            dim_inputs, dim_fs, lower_tri_indices, x, proj_params_post_draws
        )
    )

    # Sum both components with weight to FS component
    return -(is_comp + fs_comp_weight * fs_comp)


def doe_fs(
    temp_dir,
    num_hf_samples,
    lower_bounds,
    upper_bounds,
    fs_comp_weight,
    dim_fs,
    proj_params_post_draws,
    optim_restarts_seed,
    num_optim_restarts,
) -> np.ndarray:

    # ⚠ thinning
    # proj_params_post_draws = proj_params_post_draws[::10]

    # Prepare objective function and Jacobian
    # def obj_func(x):
    #     return doe_objective(
    #         fs_comp_weight, dim_fs, proj_params_post_draws, num_hf_samples, x
    #     )

    # Needed for determining the minimum distance
    lower_tri_indices = jnp.tril_indices(num_hf_samples, k=-1)

    obj_func = partial(
        doe_objective,
        fs_comp_weight,
        dim_fs,
        proj_params_post_draws,
        lower_tri_indices,
    )

    best_x = optimize_input_sites(
        obj_func,
        num_hf_samples,
        lower_bounds,
        upper_bounds,
        num_optim_restarts,
        optim_restarts_seed,
        saved_state_filepath=temp_dir / "doe_saved_state.pkl",
    )

    # if DEBUG:
    #     plot_design_in_fs(temp_dir, proj_params_post_draws, dim_fs, best_x)

    return best_x
