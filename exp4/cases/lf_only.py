from dataclasses import dataclass, replace
from pathlib import Path

from jax import config as jax_config

from exp4.elliptic_pde import get_validation_data
from exp4.models import (
    sf_ridge_gp_train,
    sf_ridge_gp_predict,
    SfRidgeGpTrainingArtifacts,
    McmcParameters,
)
from exp4.utils import Dataset, Struct

from .utils.budget import check_case_cost_and_add_case
from .utils.doe import CaseParameters, CaseType
from .utils.parameters import ProcessParameters
from .utils.sampling import lf_doe_is
from .utils.validation import (
    ValidationMetrics,
    ValidationParameters,
    compute_validation_metrics,
)

jax_config.update("jax_enable_x64", True)

##############
# Parameters #
##############


@dataclass(frozen=True)
class LFOnlyPreparationParameters(Struct):
    pass


@dataclass(frozen=True)
class LFOnlyTrainingParameters(Struct):
    mcmc: McmcParameters


LFOnlyValidationParameters = ValidationParameters


@dataclass(frozen=True)
class LfOnlyParameters(ProcessParameters):
    preparation: LFOnlyPreparationParameters
    training: LFOnlyTrainingParameters
    validation: LFOnlyValidationParameters


#############
# Artifacts #
#############


@dataclass(frozen=True)
class LFOnlyPreparationArtifacts(Struct):
    lf_training_data: Dataset


LFOnlyTrainingArtifacts = SfRidgeGpTrainingArtifacts


@dataclass(frozen=True)
class LFOnlyValidationArtifacts(Struct):
    lf_training: ValidationMetrics
    lf_validation: ValidationMetrics
    hf_actual_vs_lf_predicted: ValidationMetrics


@dataclass(frozen=True)
class LfOnlyArtifacts(Struct):
    preparation: LFOnlyPreparationArtifacts
    training: LFOnlyTrainingArtifacts
    validation: LFOnlyValidationArtifacts


############
# Routines #
############


def prepare(
    temp_dir: Path,
    preparation_parameters: LFOnlyPreparationParameters,
    case_parameters: CaseParameters,
) -> LFOnlyPreparationArtifacts:
    return LFOnlyPreparationArtifacts(
        lf_training_data=lf_doe_is(temp_dir, case_parameters)
    )


def train(
    case_temp_dir: Path,
    training_parameters: LFOnlyTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: LFOnlyPreparationArtifacts,
) -> LFOnlyTrainingArtifacts:
    return sf_ridge_gp_train(
        preparation_artifacts.lf_training_data,
        training_parameters.mcmc,
        case_parameters.dim_feature_space,
        case_temp_dir / "mcmc.pkl",
    )


def validate(
    validation_parameters: LFOnlyValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: LFOnlyPreparationArtifacts,
    training_artifacts: LFOnlyTrainingArtifacts,
) -> LFOnlyValidationArtifacts:

    # Retrieve data needed for validation
    lf_training_data = preparation_artifacts.lf_training_data
    lf_validation_data = get_validation_data(case_parameters.lf_process)
    hf_validation_data = get_validation_data(case_parameters.hf_process)

    def predict(pred_x):
        return sf_ridge_gp_predict(pred_x, lf_training_data, training_artifacts)

    def validation_routine(means, variances, actual):
        actual = actual.reshape((-1, 1))  # need a column vector
        return compute_validation_metrics(
            means, variances, actual, validation_parameters
        )

    return LFOnlyValidationArtifacts(
        lf_training=validation_routine(
            *predict(lf_training_data.x), lf_training_data.y
        ),
        lf_validation=validation_routine(
            *predict(lf_validation_data.x), lf_validation_data.y
        ),
        hf_actual_vs_lf_predicted=validation_routine(
            *predict(hf_validation_data.x), hf_validation_data.y
        ),
    )


def finalize(
    case_id: str,
    process_parameters: ProcessParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: LFOnlyPreparationArtifacts,
) -> None:
    # We spawn new DOE cases that depend on this case.
    # These are all the DOE cases for the HF sampling using a DOE,
    # either in the FS or in the original IS.
    # These cases inherit the same case parameters.

    for i, hf_doe_budget_fraction in enumerate([0.1, 0.2, 0.3, 0.4, 0.5]):
        # HF DOE in FS
        new_case_parameters = replace(
            case_parameters,
            case_type=CaseType.HF_DOE_FS,
            case_id="{}-FS{}".format(case_id, i + 1),
            parent_case_id=case_id,
            hf_doe_budget_fraction=hf_doe_budget_fraction,
        )
        check_case_cost_and_add_case(new_case_parameters)
