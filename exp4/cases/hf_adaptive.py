from dataclasses import dataclass, replace
from functools import partial
from pathlib import Path
from typing import cast

import h5py as h5
from jax import numpy as jnp, config as jax_config
import numpy as np

from exp4.elliptic_pde import evaluate_elliptic_pde
from exp4.models import mf_deep_ridge_gp_predict
from exp4.settings import RESULTS_DIR
from exp4.utils import h5group_to_struct, Dataset, Struct

from .utils.budget import check_case_cost_and_add_case, compute_num_hf_adaptive_samples
from .utils.doe import CaseParameters, CaseType
from .utils.multi_fidelity import (
    MfPreparationArtifacts,
    MfTrainingArtifacts,
    MfTrainingParameters,
    MfValidationArtifacts,
    MfValidationParameters,
    train_mf,
    validate_mf,
)
from .utils.parameters import ProcessParameters
from .utils.sampling import optimize_input_sites

jax_config.update("jax_enable_x64", True)

##############
# Parameters #
##############


@dataclass(frozen=True)
class HfAdaptivePreparationParameters(Struct):
    allocated_budget_fraction: float
    num_lf_predictions_mf_model: int
    num_optim_restarts: int
    lf_predictions_random_seed: int


HfAdaptiveTrainingParameters = MfTrainingParameters

HfAdaptiveValidationParameters = MfValidationParameters


@dataclass(frozen=True)
class HfAdaptiveParameters(ProcessParameters):
    preparation: HfAdaptivePreparationParameters
    training: HfAdaptiveTrainingParameters
    validation: HfAdaptiveValidationParameters


#############
# Artifacts #
#############

HfAdaptivePreparationArtifacts = MfPreparationArtifacts

HfAdaptiveTrainingArtifacts = MfTrainingArtifacts

HfAdaptiveValidationArtifacts = MfValidationArtifacts


def prepare(
    temp_dir: Path,
    preparation_parameters: HfAdaptivePreparationParameters,
    case_parameters: CaseParameters,
) -> HfAdaptivePreparationArtifacts:
    # Retrieve LF samples, HF samples, and previous model from parent case #############
    parent_case_id = case_parameters.parent_case_id
    with h5.File(RESULTS_DIR / "{}.h5".format(parent_case_id)) as file:
        # LF samples
        h5_lf_training_data = file["preparation_artifacts/lf_training_data"]
        assert type(h5_lf_training_data) is h5.Group
        lf_training_data = h5group_to_struct(h5_lf_training_data, Dataset)

        # HF samples
        h5_hf_training_data = file["preparation_artifacts/hf_training_data"]
        assert type(h5_hf_training_data) is h5.Group
        hf_training_data = h5group_to_struct(h5_hf_training_data, Dataset)

        # Posterior draws for the model parameters
        h5_training_artifacts = file["training_artifacts"]
        assert type(h5_training_artifacts) is h5.Group
        training_artifacts = h5group_to_struct(
            h5_training_artifacts, MfTrainingArtifacts
        )

    # Select HF training data using adaptive sampling ##################################
    # We keep the same model even though we are sampling the `num_hf_samples` HF samples
    # one-by-one.

    # Retrieve relevant case parameters
    num_hf_samples = compute_num_hf_adaptive_samples(
        case_parameters, preparation_parameters.allocated_budget_fraction
    )
    # lf_cost = get_elliptic_pde_cost(case_parameters.lf_process)
    # hf_cost = get_elliptic_pde_cost(case_parameters.hf_process)
    # remaining_budget = (
    #     case_parameters.budget
    #     - (lf_training_data.num_samples * lf_cost)
    #     - (hf_training_data.num_samples * hf_cost)
    # )
    # case_allocated_budget = (
    #     case_parameters.budget * preparation_parameters.allocated_budget_fraction
    # )
    # case_budget = min(remaining_budget, case_allocated_budget)
    # num_hf_samples = int(case_budget // hf_cost)

    # Iterate to select new HF samples one-by-one
    for _ in range(num_hf_samples):
        # Select next HF sample point using active sampling
        new_x = active_sampling_fs(
            temp_dir,
            preparation_parameters,
            case_parameters,
            lf_training_data,
            hf_training_data,
            training_artifacts,
        )

        # Evaluate the function at that point
        new_y = evaluate_elliptic_pde(temp_dir, new_x, case_parameters.hf_process)

        # Add new function evaluation to HF training set
        hf_training_data = Dataset(
            x=np.concatenate((hf_training_data.x, new_x), axis=0),
            y=np.concatenate((hf_training_data.y, new_y), axis=0),
        )

    return HfAdaptivePreparationArtifacts(
        lf_training_data=lf_training_data,
        hf_training_data=hf_training_data,
    )


def train(
    case_temp_dir: Path,
    training_parameters: HfAdaptiveTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfAdaptivePreparationArtifacts,
) -> HfAdaptiveTrainingArtifacts:
    return train_mf(
        case_temp_dir, training_parameters, case_parameters, preparation_artifacts
    )


def validate(
    validation_parameters: HfAdaptiveValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfAdaptivePreparationArtifacts,
    training_artifacts: HfAdaptiveTrainingArtifacts,
) -> HfAdaptiveValidationArtifacts:
    return validate_mf(
        validation_parameters,
        case_parameters,
        preparation_artifacts,
        training_artifacts,
    )


def finalize(
    case_id: str,
    process_parameters: ProcessParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: HfAdaptivePreparationArtifacts,
):
    # Compute remaining actual and allocated budgets for the child cases
    process_parameters = cast(HfAdaptiveParameters, process_parameters)
    next_hf_adaptive_budget_fraction = (
        case_parameters.hf_adaptive_budget_fraction
        + process_parameters.preparation.allocated_budget_fraction
    )
    next_spent_budget_fraction = (
        case_parameters.lf_doe_budget_fraction
        + case_parameters.hf_doe_budget_fraction
        + next_hf_adaptive_budget_fraction
    )

    # If we can at least sample one more HF
    if next_spent_budget_fraction <= 1.0:

        # Parameters for the new case
        new_case_parameters = replace(
            case_parameters,
            case_type=CaseType.HF_ADAPTIVE,
            case_id="{}{}".format(case_id[:-1], int(case_id[-1]) + 1),
            parent_case_id=case_id,
            hf_adaptive_budget_fraction=next_hf_adaptive_budget_fraction,
        )
        check_case_cost_and_add_case(new_case_parameters)


#####################
# Adaptive Sampling #
#####################


def active_sampling_objective(
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    training_artifacts: HfAdaptiveTrainingArtifacts,
    dim_feature_space: int,
    std_gaussian_samples: np.ndarray,
    x: np.ndarray,
) -> float:
    """Returns the predictive variance at point x."""

    # Multi-fidelity Bayesian ridge GP prediction
    _, _, hf_means, hf_variances = mf_deep_ridge_gp_predict(
        x,
        lf_training_data,
        hf_training_data,
        training_artifacts,
        dim_feature_space,
        std_gaussian_samples,
    )

    # Law of total variance
    return -(jnp.mean(hf_variances) + jnp.var(hf_means))


def active_sampling_fs(
    temp_dir: Path,
    preparation_parameters: HfAdaptivePreparationParameters,
    case_parameters: CaseParameters,
    lf_training_data: Dataset,
    hf_training_data: Dataset,
    training_artifacts: HfAdaptiveTrainingArtifacts,
) -> np.ndarray:
    # Retrieve relevant process parameters
    num_mf_lf_samples = preparation_parameters.num_lf_predictions_mf_model
    num_optim_restarts = preparation_parameters.num_optim_restarts
    mf_lf_predictions_random_seed = preparation_parameters.lf_predictions_random_seed

    # Retrieve relevant case parameters
    lower_bounds = case_parameters.hf_process.lower_bounds
    upper_bounds = case_parameters.hf_process.upper_bounds
    dim_feature_space = case_parameters.dim_feature_space
    optim_restarts_seed = case_parameters.adaptive_sampling_random_seed

    # Prepare objective function and Jacobian
    num_hf_training_samples = hf_training_data.num_samples
    num_predictions = 1

    rng = np.random.default_rng(mf_lf_predictions_random_seed)
    std_gaussian_samples = rng.standard_normal(
        size=(num_mf_lf_samples, num_hf_training_samples + num_predictions)
    )
    obj_func = partial(
        active_sampling_objective,
        lf_training_data,
        hf_training_data,
        training_artifacts,
        dim_feature_space,
        std_gaussian_samples,
    )

    # Optimize the active sampling criterion
    best_x = optimize_input_sites(
        obj_func,
        1,
        lower_bounds,
        upper_bounds,
        num_optim_restarts,
        optim_restarts_seed,
        saved_state_filepath=temp_dir / "adaptive_sampling_saved_state.pkl",
    )

    # Create plots for the experimental designs projected in the FS draws
    # if DEBUG:
    #     plot_design_in_fs(
    #         temp_dir,
    #         training_artifacts.posterior_draws["projection_parameters"],
    #         dim_feature_space,
    #         best_x,
    #     )

    return best_x
