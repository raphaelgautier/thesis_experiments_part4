from dataclasses import dataclass
from pathlib import Path
from typing import Tuple, Any

import numpy as np

# from scipy.stats import qmc

from exp4.elliptic_pde import get_validation_data
from exp4.utils import Struct

from .utils.doe import CaseParameters
from .utils.multi_fidelity import MfPreparationArtifacts, MfValidationArtifacts
from .utils.parameters import ProcessParameters
from .utils.sampling import lf_and_hf_doe_is
from .utils.validation import ValidationParameters, compute_validation_metrics


# def lhs_doe(dim_inputs, num_samples, lower_bounds, upper_bounds, doe_random_seed):
#     # Select LF training data using DOE in IS
#     return qmc.scale(
#         qmc.LatinHypercube(d=dim_inputs, seed=doe_random_seed).random(n=num_samples),
#         lower_bounds,
#         upper_bounds,
#     )


@dataclass(frozen=True)
class DeepMfGpPreparationParameters(Struct):
    pass


DeepMfGpPreparationArtifacts = MfPreparationArtifacts


@dataclass(frozen=True)
class DeepMfGpTrainingParameters(Struct):
    num_iterations: int


@dataclass(frozen=True)
class DeepMfGpTrainingArtifacts(Struct):
    model: Any  # type is really MultiFidelityDeepGP
    # but importing tensorflow on some platforms creates problems


@dataclass(frozen=True)
class DeepMfGpValidationParameters(ValidationParameters):
    num_gp_samples: int = 1000


DeepMfGpValidationArtifacts = MfValidationArtifacts


@dataclass(frozen=True)
class DeepMfGpParameters(ProcessParameters):
    preparation: DeepMfGpPreparationParameters
    training: DeepMfGpTrainingParameters
    validation: DeepMfGpValidationParameters


def prepare(
    temp_dir: Path,
    process_parameters: DeepMfGpPreparationParameters,
    case_parameters: CaseParameters,
) -> DeepMfGpPreparationArtifacts:
    # Select LF training data and HF training data using a DOE in IS
    preparation_artifacts = lf_and_hf_doe_is(temp_dir, case_parameters)
    if preparation_artifacts.hf_training_data.num_samples <= 1:
        raise RuntimeError("DMF needs strictly more than 1 HF training sample.")
    return preparation_artifacts


def train(
    case_temp_dir: Path,
    training_parameters: DeepMfGpTrainingParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: DeepMfGpPreparationArtifacts,
):
    from emukit.examples.multi_fidelity_dgp.multi_fidelity_deep_gp import (
        MultiFidelityDeepGP,
    )

    # Train Cutajar's multi-fidelity model
    X = [
        preparation_artifacts.lf_training_data.x,
        preparation_artifacts.hf_training_data.x,
    ]

    Y = [
        preparation_artifacts.lf_training_data.y,
        preparation_artifacts.hf_training_data.y,
    ]

    model = MultiFidelityDeepGP(X, Y, n_iter=training_parameters.num_iterations)
    model.optimize()

    return DeepMfGpTrainingArtifacts(model=model)


def validate(
    validation_parameters: DeepMfGpValidationParameters,
    case_parameters: CaseParameters,
    preparation_artifacts: DeepMfGpPreparationArtifacts,
    training_artifacts: DeepMfGpTrainingArtifacts,
) -> DeepMfGpValidationArtifacts:

    # Extract parameters
    num_gp_samples = validation_parameters.num_gp_samples

    def predict(
        pred_x: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        # pred_x should be a 2D array
        assert len(pred_x.shape) == 2

        _, fmeans, fvars = training_artifacts.model.model.predict_all_layers(
            pred_x, num_gp_samples
        )

        # Outputs are size num_predictions x num_gp_samples
        lf_means = fmeans[0][:, :, 0].T
        lf_variances = fvars[0][:, :, 0].T
        hf_means = fmeans[1][:, :, 0].T
        hf_variances = fvars[1][:, :, 0].T

        return lf_means, lf_variances, hf_means, hf_variances

    def validation_routine(means, variances, actual):
        actual = actual.reshape((-1, 1))  # need a column vector
        return compute_validation_metrics(
            means, variances, actual, validation_parameters
        )

    # Retrieve data needed for validation
    lf_training_data = preparation_artifacts.lf_training_data
    hf_training_data = preparation_artifacts.hf_training_data
    lf_validation_data = get_validation_data(case_parameters.lf_process)
    hf_validation_data = get_validation_data(case_parameters.hf_process)

    # Make predictions
    lf_train_means, lf_train_variances, _, _ = predict(lf_training_data.x)
    lf_val_means, lf_val_variances, _, _ = predict(lf_validation_data.x)
    _, _, hf_train_means, hf_train_variances = predict(hf_training_data.x)
    (
        lf_means_at_hf_val,
        lf_variances_at_hf_val,
        hf_val_means,
        hf_val_variances,
    ) = predict(hf_validation_data.x)

    return DeepMfGpValidationArtifacts(
        lf_training=validation_routine(
            lf_train_means, lf_train_variances, lf_training_data.y
        ),
        lf_validation=validation_routine(
            lf_val_means, lf_val_variances, lf_validation_data.y
        ),
        hf_training=validation_routine(
            hf_train_means, hf_train_variances, hf_training_data.y
        ),
        hf_validation=validation_routine(
            hf_val_means, hf_val_variances, hf_validation_data.y
        ),
        hf_actual_vs_lf_predicted=validation_routine(
            lf_means_at_hf_val, lf_variances_at_hf_val, hf_validation_data.y
        ),
    )


def finalize(
    case_id: str,
    case_parameters: CaseParameters,
    process_parameters: ProcessParameters,
    preparation_artifacts: MfPreparationArtifacts,
):
    # Nothing to do
    pass
