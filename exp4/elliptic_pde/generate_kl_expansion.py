from pathlib import Path

import numpy as np
from scipy.spatial.distance import pdist, squareform
from scipy.linalg import eigh


def generate_kl_expansion(beta, num_modes, num_points):
    # Coordinates along one axis
    x = np.linspace(0, 1, num_points)

    # Generates a list of 2D coordinates for the grid (num_points**2 x 2)
    points = np.stack(np.meshgrid(x, x), -1).reshape(-1, 2)

    # Covariance matrix
    k = np.exp(-squareform(pdist(points, metric="minkowski", p=1)) / beta)

    # Get eigenvectors (eigh returns them in ascending eigenvalue's order)
    size = num_points ** 2
    eigenvalues, eigenvectors = eigh(k, subset_by_index=[size - num_modes, size - 1])

    return eigenvalues[::-1], eigenvectors[:, ::-1]


if __name__ == "__main__":

    beta_list = [0.01, 1.0]
    num_modes = 100
    num_points = 100

    for beta in beta_list:
        eigenvalues, eigenvectors = generate_kl_expansion(beta, num_modes, num_points)
        np.savez(
            Path(__file__).parent
            / Path(
                "kl_expansion_beta_{}_modes_{}_grid_{}".format(
                    beta, num_modes, num_points
                )
            ),
            eigenvectors=eigenvectors,
            eigenvalues=eigenvalues,
        )
