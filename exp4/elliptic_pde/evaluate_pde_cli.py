import argparse
from pathlib import Path
import time

import numpy as np
import pandas as pd

from exp4.elliptic_pde.pde_model import PDESolver


def retrieve_params():
    parser = argparse.ArgumentParser(description="Evaluate Elliptic PDE")
    parser.add_argument("input_file", type=Path, help="path to the input csv")
    parser.add_argument("output_file", type=Path, help="path to the output csv")
    parser.add_argument("beta", type=float, choices=[0.01, 1.0], help="value of beta")
    parser.add_argument(
        "num_modes", type=int, choices=[10, 25, 50, 100], help="number of modes/inputs"
    )
    parser.add_argument("grid_size", type=int, choices=[32, 100], help="size of grid")
    return parser.parse_args()


def evaluate_process(
    x: np.ndarray, beta: float, num_modes: float, grid_size: float
) -> np.ndarray:
    # Dimension
    dim_inputs = x.shape[1]

    # Check
    assert (
        dim_inputs == num_modes
    ), "Input dimension ({}) and number of modes ({}) should be equal".format(
        dim_inputs, num_modes
    )

    # Evaluate elliptic PDE
    solver = PDESolver(x.shape[1], grid_size, beta)

    y = []
    durations = []
    for j in range(x.shape[0]):
        start_time = time.time()
        y.append(solver.qoi(x[j, :]))
        durations.append(time.time() - start_time)

    return np.concatenate((np.array(y)[:, None], np.array(durations)[:, None]), axis=1)


if __name__ == "__main__":

    # Retrieve params
    params = retrieve_params()

    # Read input file
    inputs = pd.read_csv(params.input_file).to_numpy()

    # Execute model
    y = evaluate_process(inputs, params.beta, params.num_modes, params.grid_size)

    # Write output file
    pd.DataFrame(y).to_csv(params.output_file, index=False)
