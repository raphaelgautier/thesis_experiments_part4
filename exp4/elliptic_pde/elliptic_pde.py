from dataclasses import dataclass
import itertools as it
from typing import Tuple, Union, List
from pathlib import Path
import subprocess

import h5py as h5
import numpy as np
import pandas as pd

from exp4.utils import Dataset, Struct
from exp4.settings import DATA_DIR, DATASETS_DIR


@dataclass(frozen=True)
class EllipticPDEParameters(Struct):
    beta: float
    num_modes: int
    grid_size: int
    lower_bounds: List[float]
    upper_bounds: List[float]

    @property
    def num_inputs(self):
        return self.num_modes


RECORDS_FILE: Path = DATA_DIR / "processes_mean_costs.csv"

_cr: Union[pd.DataFrame, None] = None


def get_elliptic_pde_cost(process: EllipticPDEParameters) -> float:
    cr = pd.read_csv(RECORDS_FILE) if _cr is None else _cr
    return float(
        cr[
            (cr["beta"] == process.beta)
            & (cr["num_modes"] == process.num_modes)
            & (cr["grid_size"] == process.grid_size)
        ]["mean_cost"].to_numpy()
    )


def evaluate_elliptic_pde(
    temp_dir: Path,
    x: np.ndarray,
    process: EllipticPDEParameters,
    return_durations=False,
) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:

    # Params
    script = Path(__file__).parent / "evaluate_pde_cli.py"
    input_file = temp_dir / "elliptic_pde_inputs.csv"
    output_file = temp_dir / "elliptic_pde_outputs.csv"

    # Write input file
    pd.DataFrame(x).to_csv(input_file, index=False)

    # Call process
    subprocess.run(
        "python {} {} {} {} {} {}".format(
            script,
            input_file,
            output_file,
            process.beta,
            process.num_modes,
            process.grid_size,
        ),
        shell=True,
        check=True,
    )

    # Read outputs
    outputs_and_durations = pd.read_csv(output_file).to_numpy()

    outputs = outputs_and_durations[:, :-1]
    durations = outputs_and_durations[:, -1]

    if return_durations:
        return outputs, durations
    else:
        return outputs


################################
# Pre-computed validation data #
################################


def get_validation_data(process: EllipticPDEParameters) -> Dataset:
    process_name = "elliptic_pde_beta_{}_modes_{}_grid_{}".format(
        process.beta, process.num_modes, process.grid_size
    )
    return load_dataset(process_name, "y")


def load_dataset(dataset_name: str, output_name: str) -> Dataset:
    with h5.File(DATASETS_DIR / "{}.h5".format(dataset_name), "r") as dataset:
        x = np.array(dataset["inputs"])

        outputs = dataset["outputs"]
        assert type(outputs) is h5.Group
        y = np.array(outputs[output_name])

        # y should be a scalar
        if len(y.shape) > 2 or (len(y.shape) == 2 and y.shape[1] > 1):
            raise RuntimeError("The output should be scalar.")

        # y should be a column vector
        y = y.reshape((-1, 1))

    return Dataset(x=x, y=y)


###########
# Helpers #
###########


def consolidate_process_costs():
    # All combinations used in exp. 4
    beta_list = [0.01, 1.0]
    num_modes_list = [10, 25, 50, 100]
    grid_size_list = [32, 100]

    # Initialize list of rows
    rows = []

    # Iterate over all combinations
    for beta, num_modes, grid_size in it.product(
        beta_list, num_modes_list, grid_size_list
    ):
        # Retrieve and open the datasets file
        filepath = DATASETS_DIR / "elliptic_pde_beta_{}_modes_{}_grid_{}.h5".format(
            beta, num_modes, grid_size
        )

        with h5.File(filepath, "r") as h5_file:
            durations = h5_file["durations"]
            assert type(durations) is h5.Dataset
            mean_cost = np.mean(durations)

        rows.append(
            {
                "beta": beta,
                "num_modes": num_modes,
                "grid_size": grid_size,
                "mean_cost": mean_cost,
            }
        )

    pd.DataFrame(rows).to_csv(RECORDS_FILE)


if __name__ == "__main__":
    consolidate_process_costs()
