from pathlib import Path

import numpad
import numpy as np
from scipy.interpolate import RectBivariateSpline


class PDESolver(object):
    def __init__(self, num_modes, grid_size, beta):
        # Prepare the grid
        eigenvalues, eigenvectors = self.prepare_grid(num_modes, grid_size, beta)

        self.B = eigenvectors * np.sqrt(eigenvalues[None, :])

        # right hand side
        self.f = 1.0

        # number of cells in spatial discretization and grid spacing
        self.nx = grid_size
        self.dx = 1.0 / self.nx

        # number of parameters
        self.m = num_modes

        self.x = None
        self.a = None

    def prepare_grid(self, num_modes, grid_size, beta):
        # Retrie the KL expansion
        kl_expansion = np.load(
            Path(__file__).parent
            / "kl_expansion_beta_{}_modes_100_grid_100.npz".format(beta)
        )
        eigenvalues = kl_expansion["eigenvalues"]
        eigenvectors = kl_expansion["eigenvectors"]

        # Problem dimensions
        kl_grid_size = int(np.sqrt(eigenvectors.shape[0]))
        num_total_modes = eigenvalues.shape[0]

        # Check dimensions
        assert beta in [
            0.01,
            1.0,
        ], "Value of `beta` must be 0.01 or 1.0 but {} was given".format(beta)
        assert grid_size <= kl_grid_size, (
            "Requested grid size ({}) is greater than the original grid size ({}), "
            "can only downsample.".format(grid_size, kl_grid_size)
        )
        assert num_modes <= num_total_modes, (
            "Requested number of modes ({}) greater than total number of modes "
            "({}).".format(num_modes, num_total_modes)
        )

        # Downsample if necessary
        if grid_size < kl_grid_size:
            kl_x = np.linspace(0, 1, kl_grid_size)
            x = np.linspace(0, 1, grid_size)
            ds_eigenvectors = np.empty((grid_size ** 2, num_modes))
            for i in range(num_modes):
                mode = eigenvectors[:, i].reshape((kl_grid_size, kl_grid_size))
                down_sampled_mode = RectBivariateSpline(kl_x, kl_x, mode)(
                    x, x, grid=True
                )
                ds_eigenvectors[:, i] = down_sampled_mode.reshape(-1)
            eigenvectors = ds_eigenvectors

        # Only retain required number of KL modes
        eigenvalues = eigenvalues[:num_modes]
        eigenvectors = eigenvectors[:, :num_modes]

        return eigenvalues, eigenvectors

    def set_coeff(self, x):
        # given value for the parameters (KL coefficients),
        # construct the coefficients of the differential operator
        _e = 2.7182818284590451
        self.x = numpad.array(x.reshape((self.m, 1)))
        a = numpad.dot(self.B, self.x)
        self.a = _e ** a.reshape((self.nx, self.nx))

    def residual(self, u):
        # compute the PDE residual for use in "solve"
        u = np.hstack([np.zeros([self.nx - 1, 1]), u, u[:, -1:]])
        u = np.vstack([np.zeros([1, self.nx + 1]), u, np.zeros([1, self.nx + 1])])
        a_hor = 0.5 * (self.a[1:, :] + self.a[:-1, :])
        a_ver = 0.5 * (self.a[:, 1:] + self.a[:, :-1])
        a_dudy = a_hor * (u[1:-1, 1:] - u[1:-1, :-1]) / self.dx
        a_dudx = a_ver * (u[1:, 1:-1] - u[:-1, 1:-1]) / self.dx
        res = (
            (a_dudy[:, 1:] - a_dudy[:, :-1]) / self.dx
            + (a_dudx[1:, :] - a_dudx[:-1, :]) / self.dx
            + self.f
        )
        return res

    def qoi(self, x):

        # set the operator coefficients with the given x
        self.set_coeff(x)

        # solve the PDE
        u = numpad.solve(
            self.residual, np.zeros([self.nx - 1, self.nx - 1]), verbose=False
        )

        # get the PDE solution on the boundary
        u_bnd = u[:, -1]

        # compute the average on the boundary
        q = 0.5 * self.dx * np.sum(u_bnd[1:] + u_bnd[:-1])

        # return perform_active_subspace_and_fbgfas number from numpad
        return numpad.value(q)

    def grad_qoi(self, x):

        # set the operator coefficients with the given x
        self.set_coeff(x)

        # solve the PDE
        u = numpad.solve(
            self.residual, np.zeros([self.nx - 1, self.nx - 1]), verbose=False
        )

        # get the PDE solution on the boundary
        u_bnd = u[:, -1]

        # compute the average on the boundary
        q = 0.5 * self.dx * np.sum(u_bnd[1:] + u_bnd[:-1])

        # return the gradient of q as perform_active_subspace_and_fbgfas numpy array
        return q.diff(self.x).todense()
