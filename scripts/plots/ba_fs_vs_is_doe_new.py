from multiprocessing import Pool
from pprint import pprint

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.elliptic_pde import get_elliptic_pde_cost, EllipticPDEParameters
from exp4.settings import DOE_DIR, FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def create_plot(group):
    # Retrieve info
    results = group["results"]
    list_dim_inputs = group["list_dim_inputs"]
    name = group["name"]
    pretty_name = group["pretty_name"]
    beta = group["beta"]

    # Create a plot
    num_input_dims = len(list_dim_inputs)
    fig_width = 10
    # fig_height = num_input_dims * 3 + 2
    fig_height = 16
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=2,
        sharex=True,
        sharey=True,
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, 2))

    # Iterate through input dimensions
    for i, dim_inputs in enumerate(list_dim_inputs):
        for j, dim_fs in enumerate(results.dim_feature_space.unique()):
            # Retrieve current axes
            ax = axes[i, j]

            # Retrieve data specific to this plot
            plot_data = results[
                (results.dim_inputs == dim_inputs)
                & (results.dim_feature_space == dim_fs)
            ].copy()

            # Add a column
            plot_data["Analysis Budget\n(in HF samples)"] = (
                results["budget"]
                / get_elliptic_pde_cost(
                    EllipticPDEParameters(
                        beta=beta,
                        num_modes=dim_inputs,
                        grid_size=100,
                        lower_bounds=[0.0],
                        upper_bounds=[1.0],
                    )
                )
            ).astype(int)

            # Rename the categorical values
            plot_data.loc[
                plot_data["case_type"] == "hf_doe_is", "case_type"
            ] = "DOE in IS"
            plot_data.loc[
                plot_data["case_type"] == "hf_doe_fs", "case_type"
            ] = "DOE in FS"

            # Rename the case type column
            plot_data.rename(columns={"case_type": "Method"}, inplace=True)

            # Draw the plot
            sns.lineplot(
                data=plot_data,
                x="hf_doe_budget_fraction",
                y="hf_r2",
                hue="Analysis Budget\n(in HF samples)",
                style="Method",
                style_order=["DOE in IS", "DOE in FS"],
                ax=ax,
            )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            ax.set_xlim([0.0, 0.5])
            ax.set_xlabel("HF DOE Budget Fraction")
            ax.set_ylim([0.0, 1.0])
            ax.set_ylabel("$R^2$")
            ax.set_title("{} inputs - {}D FS".format(dim_inputs, dim_fs))

            # Redraw custom legend
            handles, labels = ax.get_legend_handles_labels()
            if ax.get_legend() is not None:
                ax.get_legend().remove()
            if j == 1:
                ax.legend(
                    handles,
                    labels,
                    bbox_to_anchor=(1.04, 1.0),
                    loc="upper left",
                    borderaxespad=0,
                    ncol=1,
                )

    # Title for the whole plot
    plt.suptitle(pretty_name + "\n")

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save the figure
    dir = FIGURES_DIR / "doe_fs_vs_is_new"
    dir.mkdir(parents=True, exist_ok=True)
    # fig.savefig(dir / "{}.png".format(name), dpi=600)
    fig.savefig(dir / "{}.pdf".format(name))
    plt.close()


if __name__ == "__main__":
    # Load the results
    results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)
    pprint(list(results.columns))
    results = results[
        [item in ["hf_doe_fs", "hf_doe_is"] for item in results["case_type"]]
    ]

    groups = [
        {
            "name": "elliptic_pde_short",
            "pretty_name": "Elliptic PDE with $\\beta=0.01$",
            "results": results[results.beta == 0.01],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 0.01,
        },
        {
            "name": "elliptic_pde_long",
            "pretty_name": "Elliptic PDE with $\\beta=1.0$",
            "results": results[results.beta == 1.0],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 1.0,
        },
    ]

    # Generate plots in parallel
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(create_plot, groups),
                desc="Creating FS vs. IS DOE plots",
                total=len(groups),
            )
        )
