from multiprocessing import Pool
from typing import cast, Union

import h5py as h5
import pandas as pd
from tqdm import tqdm

from exp4.cases import get_all_cases, CaseParameters, CaseType
from exp4.cases.lf_only import LFOnlyValidationArtifacts, LFOnlyPreparationArtifacts
from exp4.cases.utils.multi_fidelity import (
    MfPreparationArtifacts,
    MfValidationArtifacts,
)
from exp4.elliptic_pde import get_elliptic_pde_cost
from exp4.settings import DOE_DIR, RESULTS_DIR
from exp4.utils.persistence import h5group_to_struct


def compute_actual_case_cost(
    case_parameters: CaseParameters,
    preparation_artifacts: Union[LFOnlyPreparationArtifacts, MfPreparationArtifacts],
):

    # Number of LF samples
    num_lf_samples = preparation_artifacts.lf_training_data.num_samples
    lf_process_cost = get_elliptic_pde_cost(case_parameters.lf_process)
    lf_cost = num_lf_samples * lf_process_cost

    # Number of HF samples if applicable
    if type(preparation_artifacts) is MfPreparationArtifacts:
        num_hf_samples = preparation_artifacts.hf_training_data.num_samples
        hf_process_cost = get_elliptic_pde_cost(case_parameters.hf_process)
        hf_cost = num_hf_samples * hf_process_cost
    else:
        hf_cost = 0

    return lf_cost + hf_cost


def get_row(case: CaseParameters):

    # Do not error if results file does not exist, jsut return None
    try:
        # Open results file and load validation artifacts
        # try:
        with h5.File(RESULTS_DIR / "{}.h5".format(case.case_id), "r") as results_file:
            preparation_artifacts = cast(
                h5.Group, results_file["preparation_artifacts"]
            )
            validation_artifacts = cast(h5.Group, results_file["validation_artifacts"])
            if case.case_type == CaseType.LF_ONLY:
                preparation_artifacts = h5group_to_struct(
                    preparation_artifacts, LFOnlyPreparationArtifacts
                )
                validation_artifacts = h5group_to_struct(
                    validation_artifacts, LFOnlyValidationArtifacts
                )
                lf_r2 = validation_artifacts.lf_validation.r_squared
                hf_r2 = validation_artifacts.hf_actual_vs_lf_predicted.r_squared
                lf_mlppd = validation_artifacts.lf_validation.mlppd
                hf_mlppd = validation_artifacts.hf_actual_vs_lf_predicted.mlppd
            else:
                preparation_artifacts = h5group_to_struct(
                    preparation_artifacts, MfPreparationArtifacts
                )
                validation_artifacts = h5group_to_struct(
                    validation_artifacts, MfValidationArtifacts
                )
                lf_r2 = validation_artifacts.lf_validation.r_squared
                hf_r2 = validation_artifacts.hf_validation.r_squared
                lf_mlppd = validation_artifacts.lf_validation.mlppd
                hf_mlppd = validation_artifacts.hf_validation.mlppd
        # except Exception as e:
        #     print(case.case_id)
        #     print(results)
        #     raise (e)

        case_cost = compute_actual_case_cost(case, preparation_artifacts)

        # Concatenate data
        return {
            # Case parameters
            "case_type": case.case_type.value,
            "case_id": case.case_id,
            "parent_case_id": case.parent_case_id,
            "dim_feature_space": case.dim_feature_space,
            "budget": case.budget,
            "case_cost": case_cost,
            "fraction_budget_used": case_cost / case.budget,
            "lf_doe_budget_fraction": case.lf_doe_budget_fraction,
            "hf_doe_budget_fraction": case.hf_doe_budget_fraction,
            "hf_adaptive_budget_fraction": case.hf_adaptive_budget_fraction,
            "beta": case.lf_process.beta,
            "dim_inputs": case.lf_process.num_modes,
            "lf_doe_random_seed": case.lf_doe_random_seed,
            "hf_doe_random_seed": case.hf_doe_random_seed,
            "adaptive_sampling_random_seed": case.adaptive_sampling_random_seed,
            # Case results
            "lf_r2": lf_r2,
            "hf_r2": hf_r2,
            "lf_mlppd": lf_mlppd,
            "hf_mlppd": hf_mlppd,
        }
    except FileNotFoundError:
        return None


if __name__ == "__main__":
    # Create an index of cases
    cases = get_all_cases()

    # Treat each individual results file
    with Pool() as p:
        rows = list(
            tqdm(
                p.imap_unordered(get_row, cases.values()),
                desc="Reading all results files",
                total=len(cases),
            )
        )

    # Remove missing cases
    while None in rows:
        rows.remove(None)

    # We artificially add cases to make plotting easier
    new_rows = []
    for row in rows:
        if row["case_type"] == CaseType.LF_ONLY:
            for case_type in [CaseType.HF_DOE_IS, CaseType.HF_DOE_FS]:
                new_row = row.copy()
                new_row.update({"case_type": case_type})
                new_rows.append(new_row)
    rows.extend(new_rows)

    # Save extracted data
    pd.DataFrame(rows).to_csv(DOE_DIR / "consolidated_results.csv")
