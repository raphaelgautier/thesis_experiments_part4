import itertools as it
from multiprocessing import Pool

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.cases import CaseType
from exp4.settings import DOE_DIR, FIGURES_DIR

# Load the results
results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)

# Only use FS DOE and IS DOE cases
results = results[
    (results["case_type"] == CaseType.HF_DOE_IS)
    | (results["case_type"] == CaseType.HF_DOE_FS)
    # | (results["case_type"] == CaseType.LF_ONLY)
]

# Fix the dataset
dim_inputs_list = results["dim_inputs"].unique()
num_beta_list = results["beta"].unique()


def create_plot(dim_inputs_and_beta):
    dim_inputs = dim_inputs_and_beta[0]
    beta = dim_inputs_and_beta[1]

    this_plot_results = results[
        (results["dim_inputs"] == dim_inputs) & (results["beta"] == beta)
    ]
    sns.lineplot(
        data=this_plot_results,
        x="fraction_budget_used",
        y="hf_r2",
        hue="budget",
        style="case_type",
    )
    # sns.lineplot(
    #     data=this_plot_results[
    #         (this_plot_results["case_type"] == CaseType.HF_DOE_FS)
    #         | (this_plot_results["case_type"] == CaseType.LF_ONLY)
    #     ],
    #     x="fraction_budget_used",
    #     y="hf_r2",
    #     hue="budget",
    # )
    plt.grid()
    plt.savefig(
        FIGURES_DIR / "fs_vs_is_doe" / "{}_{}.png".format(dim_inputs, beta), dpi=600
    )
    plt.close()


with Pool() as p:
    inputs = list(it.product(dim_inputs_list, num_beta_list))
    list(
        tqdm(
            p.imap_unordered(create_plot, inputs),
            desc="Creating FS vs. IS DOE plots",
            total=len(inputs),
        )
    )
