import itertools as it
from multiprocessing import Pool

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.cases import CaseType
from exp4.elliptic_pde import get_elliptic_pde_cost, EllipticPDEParameters
from exp4.settings import DOE_DIR, FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def create_plot(group):
    # Retrieve info
    results = group["results"]
    name = group["name"]
    pretty_name = group["pretty_name"]
    beta = group["beta"]
    dim_fs = group["dim_fs"]

    # Create a plot
    list_dim_inputs = [10, 25, 50, 100]
    num_input_dims = len(list_dim_inputs)
    num_budgets = 5
    fig_width = 20
    fig_height = 12
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=num_budgets,
        sharex=True,
        # sharey=True,
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, num_budgets))

    # Iterate through input dimensions
    for i, dim_inputs in enumerate(list_dim_inputs):
        this_dim = results[results.dim_inputs == dim_inputs]
        budgets = sorted(list(this_dim.budget.unique()))
        for j, budget in enumerate(budgets):
            # Retrieve current axes
            ax = axes[i, j]

            # Retrieve data specific to this plot
            plot_data = results[
                (results.dim_inputs == dim_inputs) & (results.budget == budget)
            ].copy()

            # Add a column for analysis budget in HF samples
            hf_cost = get_elliptic_pde_cost(
                EllipticPDEParameters(
                    beta=beta,
                    num_modes=dim_inputs,
                    grid_size=100,
                    lower_bounds=[0.0],
                    upper_bounds=[1.0],
                )
            )
            plot_data["Analysis Budget\n(in HF samples)"] = (
                results["budget"] / hf_cost
            ).astype(int)
            budget_hf_samples = int(budget / hf_cost)

            # Rename the categorical values
            plot_data.loc[
                plot_data["case_type"] == "hf_adaptive", "case_type"
            ] = "Adaptive Sampling"
            plot_data.loc[
                plot_data["case_type"] == "hf_doe_fs", "case_type"
            ] = "DOE in FS"

            # Rename the case type column
            plot_data.rename(columns={"case_type": "Method"}, inplace=True)

            plot_data["HF Budget Fraction"] = (
                results["hf_doe_budget_fraction"]
                + results["hf_adaptive_budget_fraction"]
            )

            # sns.boxplot(
            #     data=plot_data,
            #     x="HF Budget Fraction",
            #     y="hf_r2",
            #     hue='hf_doe_budget_fraction',
            #     ax=ax,
            # )

            # A single line for DOE in HF
            sns.lineplot(
                data=plot_data[
                    (plot_data["Method"] == "DOE in FS")
                    & (plot_data["HF Budget Fraction"] > 0)
                ],
                x="HF Budget Fraction",
                y="hf_r2",
                err_style="bars",
                ax=ax,
                linewidth=3,
                legend="full",
                hue="Method",
            )
            doe_handles, doe_labels = ax.get_legend_handles_labels()

            # One line per value of HF DOE FS budget fraction for HF adaptive
            sns.lineplot(
                data=plot_data[
                    (plot_data["HF Budget Fraction"] > 0)
                    & (plot_data["hf_doe_budget_fraction"] < 0.5)
                ],
                x="HF Budget Fraction",
                y="hf_r2",
                hue="hf_doe_budget_fraction",
                err_style="bars",
                ax=ax,
                linestyle="--",
                palette=sns.color_palette("tab10", 5)[1:],
                legend="full",
            )
            # for hf_doe_budget_fraction in plot_data.hf_doe_budget_fraction.unique():
            #     sns.lineplot(
            #         data=plot_data[
            #             (plot_data.hf_doe_budget_fraction == hf_doe_budget_fraction)
            #             & (plot_data["HF Budget Fraction"] > 0)
            #         ],
            #         x="HF Budget Fraction",
            #         y="hf_r2",
            #         err_style="bars",
            #         ax=ax,
            #         linestyle="--",
            #     )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            ax.set_xlim([0.08, 0.52])
            ax.set_xlabel("HF Budget Fraction")
            ax.set_xticks([0.1, 0.2, 0.3, 0.4, 0.5])
            ax.set_ylabel("$R^2$") if j == 0 else ax.set_ylabel(None)
            ax.set_title(
                "{} inputs\nBudget of {} HF samples".format(
                    dim_inputs, budget_hf_samples
                )
            )

            # Redraw custom legend
            handles, labels = ax.get_legend_handles_labels()
            handles = handles[1:5] + [ax.get_lines()[0]]
            labels = labels[1:5] + ["0.5 (DOE only)"]
            # handles = doe_handles + handles
            # labels = doe_labels + labels
            if ax.get_legend() is not None:
                ax.get_legend().remove()
            if (i, j) == (0, 0):
                ax.legend(
                    handles,
                    labels,
                    bbox_to_anchor=(0.0, 1.4),
                    loc="lower left",
                    borderaxespad=0,
                    ncol=2,
                    title="HF DOE Budget Fraction",
                )

    # Title for the whole plot
    plt.suptitle(pretty_name, y=0.92)

    # Tight layout to fit the legend with the plot
    plt.tight_layout(rect=(0, 0, 1, 1.05))

    # Save the figure
    dir = FIGURES_DIR / "doe_fs_vs_hf_adaptive_new"
    dir.mkdir(parents=True, exist_ok=True)
    fig.savefig(dir / "{}.png".format(name), dpi=600)
    fig.savefig(dir / "{}.pdf".format(name))
    plt.close()


if __name__ == "__main__":
    # Load the results
    results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)

    # Only use FS DOE and IS DOE cases
    results = results[
        (results["case_type"] == CaseType.HF_ADAPTIVE)
        | (results["case_type"] == CaseType.HF_DOE_FS)
    ]

    # One plot per group
    groups = []
    for beta in results.beta.unique():
        this_beta = results[results.beta == beta]
        for dim_fs in this_beta.dim_feature_space.unique():
            this_group = this_beta[this_beta.dim_feature_space == dim_fs]
            groups.append(
                {
                    "name": "elliptic_pde_beta-{}_{}D-FS".format(beta, dim_fs),
                    "pretty_name": "Elliptic PDE with $\\beta={}$ - {}D FS".format(
                        beta, dim_fs
                    ),
                    "beta": beta,
                    "dim_fs": dim_fs,
                    "results": this_group.copy(),
                }
            )

    # Create plots in parallel
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(create_plot, groups),
                desc="Creating FS DOE vs. HF adaptive plots",
                total=len(groups),
            )
        )
