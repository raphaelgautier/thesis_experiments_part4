import itertools as it
from multiprocessing import Pool
from ntpath import join

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.cases import CaseType
from exp4.elliptic_pde import get_elliptic_pde_cost, EllipticPDEParameters
from exp4.settings import DOE_DIR, FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def create_plot(group):
    # Retrieve info
    results = group["results"]
    name = group["name"]
    pretty_name = group["pretty_name"]
    beta = group["beta"]
    dim_fs = group["dim_fs"]

    # Create a plot
    list_dim_inputs = [10, 25, 50, 100]
    num_input_dims = len(list_dim_inputs)
    num_budgets = 5
    fig_width = 20
    fig_height = 12
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=num_budgets,
        sharex=True,
        # sharey="row",
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, num_budgets))

    # Iterate through input dimensions
    for i, dim_inputs in enumerate(list_dim_inputs):
        this_dim = results[results.dim_inputs == dim_inputs]
        budgets = sorted(list(this_dim.budget.unique()))
        for j, budget in enumerate(budgets):
            # Retrieve current axes
            ax = axes[i, j]

            # Retrieve data specific to this plot
            plot_data = results[
                (results.dim_inputs == dim_inputs) & (results.budget == budget)
            ].copy()

            # Add a column for analysis budget in HF samples
            hf_cost = get_elliptic_pde_cost(
                EllipticPDEParameters(
                    beta=beta,
                    num_modes=dim_inputs,
                    grid_size=100,
                    lower_bounds=[0.0],
                    upper_bounds=[1.0],
                )
            )
            plot_data["Analysis Budget\n(in HF samples)"] = (
                results["budget"] / hf_cost
            ).astype(int)
            budget_hf_samples = int(budget / hf_cost)

            # Rename the categorical values
            plot_data.loc[
                plot_data["case_type"] == "hf_adaptive", "case_type"
            ] = "Adaptive Sampling"
            plot_data.loc[
                plot_data["case_type"] == "hf_doe_fs", "case_type"
            ] = "DOE in FS"

            # Rename the case type column
            plot_data.rename(columns={"case_type": "Method"}, inplace=True)

            plot_data["HF Budget Fraction"] = (
                results["hf_doe_budget_fraction"]
                + results["hf_adaptive_budget_fraction"]
            )

            # Filter out cases that do not use the full budget
            plot_data = plot_data[plot_data["HF Budget Fraction"] == 0.5]

            def func(row):
                if row["Method"] == "DOE in FS":
                    return 0
                else:
                    return row["hf_adaptive_budget_fraction"]

            plot_data["Case"] = plot_data.loc[
                :, ["Method", "hf_adaptive_budget_fraction"]
            ].apply(func, axis=1)

            # Draw plot
            sns.boxplot(
                data=plot_data,
                x="Case",
                y="hf_r2",
                # kind="point",
                # dodge=False,
                # join=False,
                ax=ax,
            )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            ax.set_xlabel("HF Adaptive Budget Fraction") if i == 3 else ax.set_xlabel(
                None
            )
            ax.set_ylabel("$R^2$") if j == 0 else ax.set_ylabel(None)
            ax.set_title(
                "{} inputs\nBudget of {} HF samples".format(
                    dim_inputs, budget_hf_samples
                )
            )

            # Redraw custom legend
            if ax.get_legend() is not None:
                ax.get_legend().remove()

    # Title for the whole plot
    plt.suptitle(pretty_name)

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save the figure
    dir = FIGURES_DIR / "doe_fs_vs_hf_adaptive_new_max_budget"
    dir.mkdir(parents=True, exist_ok=True)
    # fig.savefig(dir / "{}.png".format(name), dpi=600)
    fig.savefig(dir / "{}.pdf".format(name))
    plt.close()


if __name__ == "__main__":
    # Load the results
    results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)

    # Only use FS DOE and IS DOE cases
    results = results[
        (results["case_type"] == CaseType.HF_ADAPTIVE)
        | (results["case_type"] == CaseType.HF_DOE_FS)
    ]

    # One plot per group
    groups = []
    for beta in results.beta.unique():
        this_beta = results[results.beta == beta]
        for dim_fs in this_beta.dim_feature_space.unique():
            this_group = this_beta[this_beta.dim_feature_space == dim_fs]
            groups.append(
                {
                    "name": "elliptic_pde_beta-{}_{}D-FS".format(beta, dim_fs),
                    "pretty_name": "Elliptic PDE with $\\beta={}$ - {}D FS".format(
                        beta, dim_fs
                    ),
                    "beta": beta,
                    "dim_fs": dim_fs,
                    "results": this_group.copy(),
                }
            )

    # Create plots in parallel
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(create_plot, groups),
                desc="Creating FS DOE vs. HF adaptive plots",
                total=len(groups),
            )
        )
