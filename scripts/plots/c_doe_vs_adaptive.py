import itertools as it
from multiprocessing import Pool

from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.cases import CaseType
from exp4.settings import DOE_DIR, FIGURES_DIR

# Load the results
results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)

# Only use FS DOE and IS DOE cases
results = results[
    (results["case_type"] == CaseType.HF_ADAPTIVE)
    | (results["case_type"] == CaseType.HF_DOE_FS)
]

# Fix the dataset
dim_inputs_list = results["dim_inputs"].unique()
num_beta_list = results["beta"].unique()


def create_plot(dim_inputs_and_beta):
    dim_inputs = dim_inputs_and_beta[0]
    beta = dim_inputs_and_beta[1]

    # Filter results corresponding to this dataset
    this_dataset_results = results[
        (results["dim_inputs"] == dim_inputs) & (results["beta"] == beta)
    ]

    # Then we create one plot per budget
    budget_list = this_dataset_results["budget"].unique()
    for budget in budget_list:
        this_plot_results = this_dataset_results[
            this_dataset_results["budget"] == budget
        ]
        sns.lineplot(
            data=this_plot_results[
                (this_plot_results["case_type"] == CaseType.HF_DOE_FS)
                | (this_plot_results["case_type"] == CaseType.HF_ADAPTIVE)
            ],
            x="fraction_budget_used",
            y="hf_r2",
            hue="hf_doe_budget_fraction",
        )
        sns.lineplot(
            data=this_plot_results[
                this_plot_results["case_type"] == CaseType.HF_DOE_FS
            ],
            x="fraction_budget_used",
            y="hf_r2",
        )
        plt.grid()
        plt.savefig(
            FIGURES_DIR
            / "doe_vs_adaptive"
            / "dim_{}-beta_{}-_budget_{}.png".format(dim_inputs, beta, budget),
            dpi=600,
        )
        plt.close()


with Pool() as p:
    inputs = list(it.product(dim_inputs_list, num_beta_list))
    list(
        tqdm(
            p.imap_unordered(create_plot, inputs),
            desc="Creating FS vs. IS DOE plots",
            total=len(inputs),
        )
    )
