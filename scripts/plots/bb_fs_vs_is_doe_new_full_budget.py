from multiprocessing import Pool
from pprint import pprint
import itertools as it

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from exp4.elliptic_pde import get_elliptic_pde_cost, EllipticPDEParameters
from exp4.settings import DOE_DIR, FIGURES_DIR

FONT_SIZE = 16
LEGEND_FONT_SIZE = 12
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc(
    "legend",
    fontsize=LEGEND_FONT_SIZE,
    title_fontsize=LEGEND_FONT_SIZE,
)  # legend items and title fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


def create_plot(group):
    # Retrieve info
    results = group["results"]
    list_dim_inputs = group["list_dim_inputs"]
    name = group["name"]
    pretty_name = group["pretty_name"]
    beta = group["beta"]
    dim_fs = group["dim_fs"]

    # Create a plot
    num_input_dims = len(list_dim_inputs)
    fig_width = 20
    fig_height = 12
    fig, axes = plt.subplots(
        nrows=num_input_dims,
        ncols=5,
        sharex=True,
        # sharey="row",
        figsize=(fig_width, fig_height),
    )
    axes = axes if isinstance(axes, np.ndarray) else np.array(axes)
    axes = np.reshape(axes, (num_input_dims, 5))

    # Copy df to be able to filter rows
    results = results.copy()

    # # Get rid of invalid values
    # results.drop(
    #     results[results.fraction_budget_used > 1.0].index,
    #     inplace=True,
    # )

    # Making a new column for individual repetitions
    results["repetitions"] = results.loc[
        :, ["lf_doe_random_seed", "hf_doe_random_seed", "adaptive_sampling_random_seed"]
    ].apply(tuple, axis=1)

    # Iterate through input dimensions
    for i, dim_inputs in enumerate(list_dim_inputs):
        this_dim = results[results.dim_inputs == dim_inputs]
        for j, budget in enumerate(this_dim.budget.unique()):

            plot_data = results[
                (results.dim_inputs == dim_inputs)
                & (results.budget == budget)
                & (results.hf_doe_budget_fraction == 0.5)
            ].copy()

            # Add a column
            hf_cost = get_elliptic_pde_cost(
                EllipticPDEParameters(
                    beta=beta,
                    num_modes=dim_inputs,
                    grid_size=100,
                    lower_bounds=[0.0],
                    upper_bounds=[1.0],
                )
            )
            budget_hf_samples = int(budget / hf_cost)

            # Rename the categorical values
            plot_data.loc[plot_data.case_type == "hf_doe_is", "case_type"] = "DOE in IS"
            plot_data.loc[plot_data.case_type == "hf_doe_fs", "case_type"] = "DOE in FS"

            # Rename the case type column
            plot_data.rename(columns={"case_type": "Method"}, inplace=True)

            # if dim_inputs == 10 and beta == 0.01:
            #     print(plot_data)

            # Draw the plot
            ax = axes[i, j]
            # sns.pointplot(
            #     x="Method",
            #     y="hf_r2",
            #     hue="repetitions",
            #     data=plot_data,
            #     kind="point",
            #     dodge=False,
            #     ax=ax,
            # )
            sns.boxplot(
                x="Method",
                y="hf_r2",
                data=plot_data,
                ax=ax,
            )

            # Add grid, set x-axis limits, title, and labels
            ax.grid()
            # ax.set_xlim([0.5, 1.0])
            ax.set_xlabel("Method") if i == 3 else ax.set_xlabel(None)
            # ax.set_ylim([0.0, 1.0])
            ax.set_ylabel("$R^2$") if j == 0 else ax.set_ylabel(None)

            ax.set_title(
                "{} inputs\nBudget of {} HF samples".format(
                    dim_inputs, budget_hf_samples
                )
            )
            ax.get_legend().remove() if ax.get_legend() is not None else ...

    # # Redraw custom legend
    # handles, labels = None, None
    # for i, j in it.product(range(num_input_dims):
    #     handles, labels = axes[i].get_legend_handles_labels()
    #     if axes[i].get_legend() is not None:
    #         axes[i].get_legend().remove()
    #     axes[i].legend(
    #         handles,
    #         labels,
    #         bbox_to_anchor=(1.04, 1.0),
    #         loc="upper left",
    #         borderaxespad=0,
    #         ncol=1,
    #     )

    # Title for the whole plot
    plt.suptitle("{}\n{}D FS\n".format(pretty_name, dim_fs))

    # Tight layout to fit the legend with the plot
    plt.tight_layout()

    # Save the figure
    dir = FIGURES_DIR / "doe_fs_vs_is_new_max_budget"
    dir.mkdir(parents=True, exist_ok=True)
    fig.savefig(dir / "{}_{}DFS.png".format(name, dim_fs), dpi=600)
    plt.savefig(dir / "{}_{}DFS.pdf".format(name, dim_fs))
    plt.close()


if __name__ == "__main__":
    # Load the results
    results = pd.read_csv(DOE_DIR / "consolidated_results.csv", index_col=0)
    # pprint(list(results.columns))
    results = results[
        [item in ["hf_doe_fs", "hf_doe_is"] for item in results["case_type"]]
    ]

    groups = [
        {
            "name": "elliptic_pde_short",
            "pretty_name": "Elliptic PDE with $\\beta=0.01$",
            "results": results[
                (results.beta == 0.01) & (results.dim_feature_space == 3)
            ],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 0.01,
            "dim_fs": 3,
        },
        {
            "name": "elliptic_pde_short",
            "pretty_name": "Elliptic PDE with $\\beta=0.01$",
            "results": results[
                (results.beta == 0.01) & (results.dim_feature_space == 5)
            ],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 0.01,
            "dim_fs": 5,
        },
        {
            "name": "elliptic_pde_long",
            "pretty_name": "Elliptic PDE with $\\beta=1.0$",
            "results": results[
                (results.beta == 1.0) & (results.dim_feature_space == 3)
            ],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 1.0,
            "dim_fs": 3,
        },
        {
            "name": "elliptic_pde_long",
            "pretty_name": "Elliptic PDE with $\\beta=1.0$",
            "results": results[
                (results.beta == 1.0) & (results.dim_feature_space == 5)
            ],
            "list_dim_inputs": [10, 25, 50, 100],
            "beta": 1.0,
            "dim_fs": 5,
        },
    ]

    # Generate plots in parallel
    with Pool() as p:
        list(
            tqdm(
                p.imap_unordered(create_plot, groups),
                desc="Creating FS vs. IS DOE plots",
                total=len(groups),
            )
        )
