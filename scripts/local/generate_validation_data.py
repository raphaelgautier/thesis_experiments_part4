import itertools as it

import h5py as h5
from scipy import stats
from tqdm import tqdm

from exp4.elliptic_pde import EllipticPDEParameters, evaluate_elliptic_pde
from exp4.settings import DATASETS_DIR
from exp4.utils import pydict_to_h5group


if __name__ == "__main__":
    # Temp dir for evaluating Elliptic PDE
    temp_dir = DATASETS_DIR / "temp"
    temp_dir.mkdir(exist_ok=True)

    # Params
    doe_random_seed = 765
    num_samples = 2000
    list_beta_list = [0.01, 1.0]
    list_num_modes = [10, 25, 50, 100]
    list_grid_sizes = [32, 100]

    # Create an LHS

    for beta, num_modes, grid_size in tqdm(
        list(it.product(list_beta_list, list_num_modes, list_grid_sizes))
    ):
        # Size depends on num_modes
        lower_bounds = [-2.0] * num_modes
        upper_bounds = [2.0] * num_modes

        # The elliptic PDE process
        process = EllipticPDEParameters(
            beta=beta,
            num_modes=num_modes,
            grid_size=grid_size,
            lower_bounds=lower_bounds,
            upper_bounds=upper_bounds,
        )

        # Sampling locations
        lhs_sampler = stats.qmc.LatinHypercube(d=num_modes, seed=doe_random_seed)
        x = lhs_sampler.random(n=num_samples)
        x = stats.qmc.scale(x, lower_bounds, upper_bounds)

        # Retrieve evaluations and durations
        y, durations = evaluate_elliptic_pde(
            temp_dir, x, process, return_durations=True
        )

        # Save the results
        filename = f"elliptic_pde_beta_{beta}_modes_{num_modes}_grid_{grid_size}.h5"
        with h5.File(DATASETS_DIR / filename, "x") as h5_file:
            pydict_to_h5group(
                {"inputs": x, "outputs": {"y": y}, "durations": durations}, h5_file
            )
