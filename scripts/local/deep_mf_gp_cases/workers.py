from functools import partial
from multiprocessing import Pool

from exp4.cases import CaseType
from exp4.doe import worker

# Parameters
num_workers = 2
case_type_filter = [CaseType.DEEP_MF_GP]

# Run the workers
worker("1", clean_up=False, case_type_filter=case_type_filter)
# list(
#     Pool(num_workers).imap_unordered(
#         partial(worker, clean_up=False, case_type_filter=case_type_filter),
#         range(num_workers),
#     )
# )
