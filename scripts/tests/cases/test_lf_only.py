from exp4.cases.lf_only import (
    LFOnlyPreparationParameters,
    LFOnlyTrainingParameters,
    LFOnlyValidationParameters,
)
from exp4.doe import run_case
from exp4.cases import CaseParameters, CaseType, LfOnlyParameters
from exp4.elliptic_pde.elliptic_pde import EllipticPDEParameters, get_elliptic_pde_cost
from exp4.models import McmcParameters
from exp4.utils import compact_timestamp

###########
# Process #
###########

mcmc_parameters = McmcParameters(
    target_acceptance_probability=0.8,
    num_warmup_draws=10,
    num_posterior_draws=20,
    random_seed=0,
    num_draws_between_saves=10,
    progress_bar=True,
    display_summary=True,
)

process_parameters = LfOnlyParameters(
    preparation=LFOnlyPreparationParameters(),
    training=LFOnlyTrainingParameters(mcmc=mcmc_parameters),
    validation=LFOnlyValidationParameters(
        random_seed=867, num_draws_per_gaussian=10, ci_bounds=(0.025, 0.975)
    ),
)

########
# Case #
########

case_id = "case_{}".format(compact_timestamp())
worker_id = "worker_{}".format(compact_timestamp())

lower_bounds = [-2.0] * 25
upper_bounds = [2.0] * 25
lf_process = EllipticPDEParameters(
    beta=1.0,
    num_modes=25,
    grid_size=32,
    lower_bounds=lower_bounds,
    upper_bounds=upper_bounds,
)
hf_process = EllipticPDEParameters(
    beta=1.0,
    num_modes=25,
    grid_size=100,
    lower_bounds=lower_bounds,
    upper_bounds=upper_bounds,
)
initial_budget = 15 * get_elliptic_pde_cost(hf_process)

case_parameters = CaseParameters(
    # Case identification
    case_type=CaseType.LF_ONLY,
    case_id=case_id,
    parent_case_id=None,
    # Feature space
    dim_feature_space=3,
    # Budget and budget allocation
    budget=initial_budget,
    lf_doe_budget_fraction=0.5,
    hf_doe_budget_fraction=0,
    hf_adaptive_budget_fraction=0,
    # Test Process
    lf_process=lf_process,
    hf_process=hf_process,
    # Random seeds
    lf_doe_random_seed=786,
    hf_doe_random_seed=502,
    adaptive_sampling_random_seed=396,
)

run_case(case_id, worker_id, process_parameters, case_parameters)
