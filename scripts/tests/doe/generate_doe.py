"""
In this script, we are creating an initial test DOE.
The iniital DOE only contains LF DOE, HF DOE in IS, and deep MF GP cases since HF DOE
in FS and HF adaptive are generated as children of resp. LF DOE and HF DOE in FS cases.
"""
from dataclasses import replace
import itertools as it

from tqdm import tqdm

from exp4.cases import (
    CaseParameters,
    CaseType,
    LfOnlyParameters,
    HfDoeIsParameters,
    HfDoeFsParameters,
    HfAdaptiveParameters,
    DeepMfGpParameters,
)
from exp4.cases.lf_only import (
    LFOnlyPreparationParameters,
    LFOnlyTrainingParameters,
    LFOnlyValidationParameters,
)
from exp4.cases.hf_doe_is import (
    HfDoeIsPreparationParameters,
    HfDoeIsTrainingParameters,
    HfDoeIsValidationParameters,
)
from exp4.cases.hf_doe_fs import (
    HfDoeFsPreparationParameters,
    HfDoeFsTrainingParameters,
    HfDoeFsValidationParameters,
)
from exp4.cases.hf_adaptive import (
    HfAdaptivePreparationParameters,
    HfAdaptiveTrainingParameters,
    HfAdaptiveValidationParameters,
)
from exp4.cases.deep_mf_gp import (
    DeepMfGpPreparationParameters,
    DeepMfGpTrainingParameters,
    DeepMfGpValidationParameters,
)
from exp4.cases.utils.budget import compute_total_case_cost

from exp4.doe import AllProcessParameters, initialize_doe
from exp4.elliptic_pde import EllipticPDEParameters, get_elliptic_pde_cost
from exp4.models import McmcParameters

######################
# Process parameters #
######################

# Common
mcmc_parameters = McmcParameters(
    target_acceptance_probability=0.8,
    num_warmup_draws=50,
    num_posterior_draws=100,
    random_seed=57,
    num_draws_between_saves=200,
    progress_bar=False,
    display_summary=False,
)
validation_random_seed = 867
lf_predictions_seed_mf_model = 398
ci_bounds = (0.025, 0.975)

# LF only
lf_only = LfOnlyParameters(
    preparation=LFOnlyPreparationParameters(),
    training=LFOnlyTrainingParameters(mcmc=mcmc_parameters),
    validation=LFOnlyValidationParameters(
        random_seed=validation_random_seed,
        num_draws_per_gaussian=10,
        ci_bounds=ci_bounds,
    ),
)

# HF DOE FS
hf_doe_fs = HfDoeFsParameters(
    preparation=HfDoeFsPreparationParameters(
        fs_component_optim_weight=100, num_optim_restarts=1
    ),
    training=HfDoeFsTrainingParameters(mcmc=mcmc_parameters),
    validation=HfDoeFsValidationParameters(
        random_seed=validation_random_seed,
        num_draws_per_gaussian=10,
        ci_bounds=ci_bounds,
        num_lf_predictions_mf_model=10,
        lf_predictions_seed_mf_model=lf_predictions_seed_mf_model,
    ),
)

# HF DOE IS
hf_doe_is = HfDoeIsParameters(
    preparation=HfDoeIsPreparationParameters(),
    training=HfDoeIsTrainingParameters(mcmc=mcmc_parameters),
    validation=HfDoeIsValidationParameters(
        random_seed=validation_random_seed,
        num_draws_per_gaussian=10,
        ci_bounds=ci_bounds,
        num_lf_predictions_mf_model=10,
        lf_predictions_seed_mf_model=lf_predictions_seed_mf_model,
    ),
)

# HF adaptive
hf_adaptive = HfAdaptiveParameters(
    preparation=HfAdaptivePreparationParameters(
        allocated_budget_fraction=0.1,
        num_lf_predictions_mf_model=10,
        num_optim_restarts=1,
        lf_predictions_random_seed=lf_predictions_seed_mf_model,
    ),
    training=HfAdaptiveTrainingParameters(mcmc=mcmc_parameters),
    validation=HfAdaptiveValidationParameters(
        random_seed=validation_random_seed,
        num_draws_per_gaussian=10,
        ci_bounds=ci_bounds,
        num_lf_predictions_mf_model=10,
        lf_predictions_seed_mf_model=lf_predictions_seed_mf_model,
    ),
)

# Deep MF GP
deep_mf_gp = DeepMfGpParameters(
    preparation=DeepMfGpPreparationParameters(),
    training=DeepMfGpTrainingParameters(num_iterations=5),
    validation=DeepMfGpValidationParameters(
        random_seed=validation_random_seed,
        num_draws_per_gaussian=10,
        ci_bounds=ci_bounds,
        num_gp_samples=1000,
    ),
)

# All of them
process_parameters = AllProcessParameters(
    lf_only=lf_only,
    hf_doe_is=hf_doe_is,
    hf_doe_fs=hf_doe_fs,
    hf_adaptive=hf_adaptive,
    deep_mf_gp=deep_mf_gp,
)

##########################
# Lists of Alternatives  #
# used to generate cases #
##########################

list_dim_feature_space = [3, 5]
list_beta = [0.01, 1.0]
list_num_modes = [10, 25, 50, 100]
list_random_seeds = [
    (678, 36, 274),
    (108, 855, 311),
    (546, 711, 254),
    (359, 746, 537),
    (268, 879, 158),
]
list_budget_in_hf_samples = {
    10: [5, 10, 15, 20, 25],
    25: [10, 20, 30, 40, 50],
    50: [20, 40, 60, 80, 100],
    100: [50, 90, 130, 170, 210],
}
list_hf_doe_budget_fraction = [0.1, 0.2, 0.3, 0.4, 0.5]

# Constants for all cases
lf_doe_budget_fraction = 0.5

# Initialization
case_id = 0
cases = []

combinations = list(
    it.product(list_dim_feature_space, list_beta, list_num_modes, list_random_seeds)
)

for dim_feature_space, beta, num_modes, random_seeds in tqdm(combinations):

    lower_bounds = [-2.0] * num_modes
    upper_bounds = [2.0] * num_modes

    for budget_in_hf_samples in list_budget_in_hf_samples[num_modes]:
        lf_process = EllipticPDEParameters(
            beta=beta,
            num_modes=num_modes,
            grid_size=32,
            lower_bounds=lower_bounds,
            upper_bounds=upper_bounds,
        )

        hf_process = EllipticPDEParameters(
            beta=beta,
            num_modes=num_modes,
            grid_size=100,
            lower_bounds=lower_bounds,
            upper_bounds=upper_bounds,
        )

        initial_budget = get_elliptic_pde_cost(hf_process) * budget_in_hf_samples

        # LF DOE
        case = CaseParameters(
            # Case identification
            case_type=CaseType.LF_ONLY,
            case_id="{}-LF".format(case_id),
            parent_case_id=None,
            # Feature space
            dim_feature_space=dim_feature_space,
            # Budget and budget allocation
            budget=initial_budget,
            case_cost=0,
            lf_doe_budget_fraction=0.5,
            hf_doe_budget_fraction=0,
            hf_adaptive_budget_fraction=0,
            # Test Process
            lf_process=lf_process,
            hf_process=hf_process,
            # Random seeds
            lf_doe_random_seed=random_seeds[0],
            hf_doe_random_seed=random_seeds[1],
            adaptive_sampling_random_seed=random_seeds[2],
        )
        cases.append(replace(case, case_cost=compute_total_case_cost(case)))

        # HF DOE in IS
        for i, hf_doe_budget_fraction in enumerate(list_hf_doe_budget_fraction):
            case = CaseParameters(
                # Case identification
                case_type=CaseType.HF_DOE_IS,
                case_id="{}-IS{}".format(case_id, i + 1),
                parent_case_id=str(case_id),
                # Feature space
                dim_feature_space=dim_feature_space,
                # Budget and budget allocation
                budget=initial_budget,
                case_cost=0,
                lf_doe_budget_fraction=0.5,
                hf_doe_budget_fraction=hf_doe_budget_fraction,
                hf_adaptive_budget_fraction=0,
                # Test Process
                lf_process=lf_process,
                hf_process=hf_process,
                # Random seeds
                lf_doe_random_seed=random_seeds[0],
                hf_doe_random_seed=random_seeds[1],
                adaptive_sampling_random_seed=random_seeds[2],
            )
            cases.append(replace(case, case_cost=compute_total_case_cost(case)))

        # Deep MF GP
        case = CaseParameters(
            # Case identification
            case_type=CaseType.DEEP_MF_GP,
            case_id="{}-DMF".format(case_id),
            parent_case_id=str(case_id),
            # Feature space
            dim_feature_space=dim_feature_space,
            # Budget and budget allocation
            budget=initial_budget,
            case_cost=0,
            lf_doe_budget_fraction=0.5,
            hf_doe_budget_fraction=0.5,
            hf_adaptive_budget_fraction=0,
            # Test Process
            lf_process=lf_process,
            hf_process=hf_process,
            # Random seeds
            lf_doe_random_seed=random_seeds[0],
            hf_doe_random_seed=random_seeds[1],
            adaptive_sampling_random_seed=random_seeds[2],
        )
        cases.append(replace(case, case_cost=compute_total_case_cost(case)))

        # Increment case ID
        case_id += 1

# Do the actual initialization
initialize_doe(process_parameters, cases)
