import sys

from exp4.cases import CaseType
from exp4.doe import worker


if __name__ == "__main__":
    # Creating a UID for the worker
    pbs_job_id = sys.argv[1].replace(".sched-torque.pace.gatech.edu", "")
    worker_number = sys.argv[2]
    worker_uid = "{}[{}]".format(pbs_job_id, worker_number)

    worker(
        worker_uid,
        # case_type_filter=[
        #     CaseType.LF_ONLY,
        #     CaseType.DEEP_MF_GP,
        #     CaseType.HF_DOE_IS,
        #     CaseType.HF_DOE_FS,
        # ],
    )
