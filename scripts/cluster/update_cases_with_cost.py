from dataclasses import replace
import json

from tqdm import tqdm

from exp4.cases import CaseParameters, CaseType
from exp4.cases.utils.budget import compute_total_case_cost
from exp4.doe import get_process_parameters
from exp4.settings import CASES_DIR
from exp4.utils import pydict_to_struct, struct_to_pydict

process_parameters = get_process_parameters(CaseType.HF_ADAPTIVE)
hf_adaptive_budget_step = process_parameters.preparation.allocated_budget_fraction


for case_filepath in tqdm(list(CASES_DIR.glob("*.json"))):
    with open(case_filepath, "r") as case_file:
        pydict = json.load(case_file)

    # Set a cost of zero to be able to retrieve the case
    pydict["case_cost"] = 0
    case_parameters = pydict_to_struct(pydict, CaseParameters)

    # Compute the actual case cost
    case_cost = compute_total_case_cost(
        case_parameters, hf_adaptive_budget_step=hf_adaptive_budget_step
    )
    case_parameters = replace(case_parameters, case_cost=case_cost)
    with open(case_filepath, "w") as case_file:
        json.dump(struct_to_pydict(case_parameters), case_file)
