from pathlib import Path

from exp4.doe.execution import _get_cases_by_state, _change_state, CaseState
from exp4.settings import RESULTS_DIR

cases_with_results = [file.stem for file in RESULTS_DIR.glob("*.h5")]

for case_id in _get_cases_by_state(CaseState.UNSTARTED):
    if case_id in cases_with_results:
        _change_state(case_id, CaseState.UNSTARTED, CaseState.FINISHED)
